﻿using ProcessManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FireManager;
using LogProvider;
using log4net;
using ParserManager;
using ParserManager.Model;
using System.IO;
using System.Net;
using OneSignal.CSharp.SDK;
using OneSignal.CSharp.SDK.Resources.Notifications;
using OneSignal.CSharp.SDK.Resources;

namespace KapMaster
{
    class NotificationProcess : IProcess
    {
        public bool Pause { get; set; }
        private IDBase firebase;
        ILog log = LogManager.GetLogger(typeof(NotificationProcess));
        public event Action<string, ELogType> OnLogEvent;

        private static NotificationProcess notificationProcess;
        private static object islocked = new object();
        private OneSignalClient client = null;

        public string OSAppID { get; set; }
        public string OSRestApiKey { get; set; }

        public static NotificationProcess GetInstance(string restApiKey)
        {
            if (notificationProcess == null)
            {
                lock (islocked)
                {
                    if (notificationProcess == null)
                    {
                        notificationProcess = new NotificationProcess(restApiKey);
                    }
                }
            }

            return notificationProcess;
        }
        public SortedDictionary<string, CategoryModel> Categories { get; set; }
        public Dictionary<string, List<int>> CompanyToCategories { get; set; }

        private NotificationProcess(string restApiKey)
        {
            firebase = Provider.Execute();
            client = new OneSignalClient(restApiKey);
        }

        public bool Start()
        {
            //Process();
            return false;
        }

        public bool Process(KapModel kapModel)
        {
            try
            {
                try
                {
                    ScreenLog("Notification Process Started", ELogType.Information);
                    var users = firebase.Get<UserModel>(KapTable.User.ToString());
                    string[] shortNames = kapModel.ShortName.Split(',');
                    List<UserModel> userList = new List<UserModel>();
                    foreach (var name in shortNames)
                    { 
                        userList.AddRange(users.Select(i => i.Value).Where(d => d.Company.Keys.Contains(name.Trim())).ToList());
                    }
                    List<string> tokens = userList.Select(x => x.Token).Distinct().ToList();
                    foreach (var token in tokens)
                    {
                        //firebase.SendNotification(user.DeviceToken, kapModel.Description,kapModel.ModalHeader,"", user.IsIOS);
                        var options = new NotificationCreateOptions();
                        options.AppId = new Guid(OSAppID);
                        options.IncludedSegments = new List<string> { "All" };
                        options.Contents.Add(LanguageCodes.English, kapModel.DetailDescription);
                        options.Subtitle = new Dictionary<string, string>();
                        options.Subtitle.Add(LanguageCodes.English, kapModel.LongName);
                        options.Headings = new Dictionary<string, string>();
                        options.Headings.Add(LanguageCodes.English, kapModel.ModalHeader);
                        options.IosAttachments = new Dictionary<string, string>();
                        options.IosAttachments.Add("id1", kapModel.LogoLink);
                        client.Notifications.Create(options);
                    }

                }
                catch (Exception ex)
                {
                    ScreenLog(ex.ToString(), ELogType.Information);
                    log.Error(ex);
                }
                ScreenLog("Category Process Completed", ELogType.Information);
                return true;
            }
            catch (Exception ex)
            {
                ScreenLog(typeof(CompanyProcess).ToString() + " " + ex.ToString(), ELogType.Stop);
                log.Error(ex);
                return false;
            }
        }

        public bool Stop()
        {
            try
            {
                Pause = true;
                return Pause;
            }
            catch
            {
                return false;
            }
        }

        private void ScreenLog(string message, ELogType process)
        {
            OnLogEvent?.Invoke(message, process);
        }
    }

    public class FCMResponse
    {
        public long multicast_id { get; set; }
        public int success { get; set; }
        public int failure { get; set; }
        public int canonical_ids { get; set; }
        public List<FCMResult> results { get; set; }
    }

    public class FCMResult
    {
        public string message_id { get; set; }
    }
}
