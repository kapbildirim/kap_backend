﻿namespace KapMaster
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.header = new Controls.HeaderPanel();
            this.lstLog = new System.Windows.Forms.ListBox();
            this.btnStart = new System.Windows.Forms.Button();
            this.txtFireBasePath = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtAuthKey = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.grouper1 = new GroupBoxControl.Grouper();
            this.label10 = new System.Windows.Forms.Label();
            this.txtNotificationKey = new System.Windows.Forms.TextBox();
            this.grpMessageType = new GroupBoxControl.Grouper();
            this.grouper6 = new GroupBoxControl.Grouper();
            this.txtUserPeriod = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtUserCount = new System.Windows.Forms.TextBox();
            this.grouper3 = new GroupBoxControl.Grouper();
            this.grouper5 = new GroupBoxControl.Grouper();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtBISTSirketleriPYS = new System.Windows.Forms.TextBox();
            this.txtCompaniesPeriod = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtCompanyLink = new System.Windows.Forms.TextBox();
            this.txtBISTSirketleriYK = new System.Windows.Forms.TextBox();
            this.grouper4 = new GroupBoxControl.Grouper();
            this.txtTodayToHistoryPeriod = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.grouper2 = new GroupBoxControl.Grouper();
            this.label3 = new System.Windows.Forms.Label();
            this.txtKapBaseLink = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtKapLink = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtLastNotId = new System.Windows.Forms.TextBox();
            this.btnStop = new System.Windows.Forms.Button();
            this.headerPanel1 = new Controls.HeaderPanel();
            this.txtSecurityCode = new System.Windows.Forms.TextBox();
            this.grouper7 = new GroupBoxControl.Grouper();
            this.label12 = new System.Windows.Forms.Label();
            this.txtOSAppID = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtOSRestApiKey = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.header.SuspendLayout();
            this.grouper1.SuspendLayout();
            this.grpMessageType.SuspendLayout();
            this.grouper6.SuspendLayout();
            this.grouper3.SuspendLayout();
            this.grouper5.SuspendLayout();
            this.grouper4.SuspendLayout();
            this.grouper2.SuspendLayout();
            this.headerPanel1.SuspendLayout();
            this.grouper7.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.configToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1053, 33);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(50, 29);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // configToolStripMenuItem
            // 
            this.configToolStripMenuItem.Name = "configToolStripMenuItem";
            this.configToolStripMenuItem.Size = new System.Drawing.Size(77, 29);
            this.configToolStripMenuItem.Text = "Config";
            // 
            // header
            // 
            this.header.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.header.Controls.Add(this.lstLog);
            this.header.Dock = System.Windows.Forms.DockStyle.Fill;
            this.header.HeaderColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(205)))), ((int)(((byte)(219)))));
            this.header.HeaderColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(180)))), ((int)(((byte)(209)))));
            this.header.HeaderFont = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.header.HeaderHeight = 24;
            this.header.HeaderText = "Messages";
            this.header.Icon = null;
            this.header.IconTransparentColor = System.Drawing.Color.White;
            this.header.Location = new System.Drawing.Point(0, 616);
            this.header.Margin = new System.Windows.Forms.Padding(8, 6, 8, 6);
            this.header.Name = "header";
            this.header.Padding = new System.Windows.Forms.Padding(8, 43, 8, 6);
            this.header.Size = new System.Drawing.Size(1053, 261);
            this.header.TabIndex = 31;
            // 
            // lstLog
            // 
            this.lstLog.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lstLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstLog.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstLog.FormattingEnabled = true;
            this.lstLog.HorizontalScrollbar = true;
            this.lstLog.ItemHeight = 20;
            this.lstLog.Location = new System.Drawing.Point(8, 43);
            this.lstLog.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.lstLog.Name = "lstLog";
            this.lstLog.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.lstLog.Size = new System.Drawing.Size(1037, 212);
            this.lstLog.TabIndex = 5;
            // 
            // btnStart
            // 
            this.btnStart.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btnStart.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(180)))), ((int)(((byte)(209)))));
            this.btnStart.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(180)))), ((int)(((byte)(209)))));
            this.btnStart.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(180)))), ((int)(((byte)(209)))));
            this.btnStart.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStart.Font = new System.Drawing.Font("Segoe UI Semibold", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnStart.Location = new System.Drawing.Point(735, 477);
            this.btnStart.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(118, 42);
            this.btnStart.TabIndex = 9;
            this.btnStart.Tag = "ApplicationMessageRequest";
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // txtFireBasePath
            // 
            this.txtFireBasePath.Location = new System.Drawing.Point(114, 38);
            this.txtFireBasePath.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtFireBasePath.Name = "txtFireBasePath";
            this.txtFireBasePath.Size = new System.Drawing.Size(368, 26);
            this.txtFireBasePath.TabIndex = 42;
            this.txtFireBasePath.Text = "https://kap-pro.firebaseio.com/";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label4.Location = new System.Drawing.Point(18, 73);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(79, 22);
            this.label4.TabIndex = 47;
            this.label4.Text = "Auth Key";
            // 
            // txtAuthKey
            // 
            this.txtAuthKey.Location = new System.Drawing.Point(114, 71);
            this.txtAuthKey.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtAuthKey.Name = "txtAuthKey";
            this.txtAuthKey.Size = new System.Drawing.Size(368, 26);
            this.txtAuthKey.TabIndex = 46;
            this.txtAuthKey.Text = "oUlbAYJing8mrVTI0BNBW781rQkWIjjEqTvZY9uv";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label14.Location = new System.Drawing.Point(18, 42);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(80, 22);
            this.label14.TabIndex = 45;
            this.label14.Text = "Base Link";
            // 
            // grouper1
            // 
            this.grouper1.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(228)))), ((int)(((byte)(242)))));
            this.grouper1.BackgroundGradientColor = System.Drawing.Color.White;
            this.grouper1.BackgroundGradientMode = GroupBoxControl.Grouper.GroupBoxGradientMode.None;
            this.grouper1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(180)))), ((int)(((byte)(209)))));
            this.grouper1.BorderThickness = 1F;
            this.grouper1.Controls.Add(this.label10);
            this.grouper1.Controls.Add(this.txtNotificationKey);
            this.grouper1.Controls.Add(this.label4);
            this.grouper1.Controls.Add(this.txtAuthKey);
            this.grouper1.Controls.Add(this.label14);
            this.grouper1.Controls.Add(this.txtFireBasePath);
            this.grouper1.CustomGroupBoxColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(180)))), ((int)(((byte)(209)))));
            this.grouper1.GroupImage = null;
            this.grouper1.GroupTitle = "Firebase Parameters";
            this.grouper1.Location = new System.Drawing.Point(15, 36);
            this.grouper1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.grouper1.Name = "grouper1";
            this.grouper1.Padding = new System.Windows.Forms.Padding(30, 31, 30, 31);
            this.grouper1.PaintGroupBox = false;
            this.grouper1.RoundCorners = 4;
            this.grouper1.ShadowColor = System.Drawing.Color.DarkGray;
            this.grouper1.ShadowControl = false;
            this.grouper1.ShadowThickness = 1;
            this.grouper1.Size = new System.Drawing.Size(496, 164);
            this.grouper1.TabIndex = 5;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label10.Location = new System.Drawing.Point(19, 105);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(122, 22);
            this.label10.TabIndex = 49;
            this.label10.Text = "Messaging Key";
            // 
            // txtNotificationKey
            // 
            this.txtNotificationKey.Location = new System.Drawing.Point(149, 103);
            this.txtNotificationKey.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtNotificationKey.Name = "txtNotificationKey";
            this.txtNotificationKey.Size = new System.Drawing.Size(333, 26);
            this.txtNotificationKey.TabIndex = 48;
            this.txtNotificationKey.Text = "AAAAitSv2rU:APA91bHw_NCnBCbpvXkxXSa74qWx8T-duhz_H8TEQtgJ401_io7-o0hndLx_f7o77ZIOG" +
    "ygq-jO_mDoDqAFaYct_iegpWgICKAJlHTeK7LE9Q2A_bqpnJWsyo37OhhJwc-LYe0qvuZYz";
            // 
            // grpMessageType
            // 
            this.grpMessageType.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(228)))), ((int)(((byte)(242)))));
            this.grpMessageType.BackgroundGradientColor = System.Drawing.Color.White;
            this.grpMessageType.BackgroundGradientMode = GroupBoxControl.Grouper.GroupBoxGradientMode.None;
            this.grpMessageType.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(180)))), ((int)(((byte)(209)))));
            this.grpMessageType.BorderThickness = 1F;
            this.grpMessageType.Controls.Add(this.grouper7);
            this.grpMessageType.Controls.Add(this.grouper6);
            this.grpMessageType.Controls.Add(this.grouper3);
            this.grpMessageType.Controls.Add(this.grouper2);
            this.grpMessageType.Controls.Add(this.btnStop);
            this.grpMessageType.Controls.Add(this.grouper1);
            this.grpMessageType.Controls.Add(this.btnStart);
            this.grpMessageType.CustomGroupBoxColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(180)))), ((int)(((byte)(209)))));
            this.grpMessageType.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpMessageType.GroupImage = null;
            this.grpMessageType.GroupTitle = "Message Type";
            this.grpMessageType.Location = new System.Drawing.Point(8, 43);
            this.grpMessageType.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.grpMessageType.Name = "grpMessageType";
            this.grpMessageType.Padding = new System.Windows.Forms.Padding(30, 31, 30, 31);
            this.grpMessageType.PaintGroupBox = false;
            this.grpMessageType.RoundCorners = 4;
            this.grpMessageType.ShadowColor = System.Drawing.Color.DarkGray;
            this.grpMessageType.ShadowControl = false;
            this.grpMessageType.ShadowThickness = 1;
            this.grpMessageType.Size = new System.Drawing.Size(1037, 534);
            this.grpMessageType.TabIndex = 5;
            // 
            // grouper6
            // 
            this.grouper6.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(228)))), ((int)(((byte)(242)))));
            this.grouper6.BackgroundGradientColor = System.Drawing.Color.White;
            this.grouper6.BackgroundGradientMode = GroupBoxControl.Grouper.GroupBoxGradientMode.None;
            this.grouper6.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(180)))), ((int)(((byte)(209)))));
            this.grouper6.BorderThickness = 1F;
            this.grouper6.Controls.Add(this.txtUserPeriod);
            this.grouper6.Controls.Add(this.label11);
            this.grouper6.Controls.Add(this.label13);
            this.grouper6.Controls.Add(this.txtUserCount);
            this.grouper6.CustomGroupBoxColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(180)))), ((int)(((byte)(209)))));
            this.grouper6.GroupImage = null;
            this.grouper6.GroupTitle = "User Process";
            this.grouper6.Location = new System.Drawing.Point(519, 184);
            this.grouper6.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.grouper6.Name = "grouper6";
            this.grouper6.Padding = new System.Windows.Forms.Padding(30, 31, 30, 31);
            this.grouper6.PaintGroupBox = false;
            this.grouper6.RoundCorners = 4;
            this.grouper6.ShadowColor = System.Drawing.Color.DarkGray;
            this.grouper6.ShadowControl = false;
            this.grouper6.ShadowThickness = 1;
            this.grouper6.Size = new System.Drawing.Size(496, 99);
            this.grouper6.TabIndex = 13;
            // 
            // txtUserPeriod
            // 
            this.txtUserPeriod.Location = new System.Drawing.Point(135, 37);
            this.txtUserPeriod.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtUserPeriod.Name = "txtUserPeriod";
            this.txtUserPeriod.Size = new System.Drawing.Size(110, 26);
            this.txtUserPeriod.TabIndex = 51;
            this.txtUserPeriod.Text = "10";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label11.Location = new System.Drawing.Point(18, 39);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(109, 22);
            this.label11.TabIndex = 50;
            this.label11.Text = "Period: (min)";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label13.Location = new System.Drawing.Point(18, 70);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(94, 22);
            this.label13.TabIndex = 45;
            this.label13.Text = "User Count";
            // 
            // txtUserCount
            // 
            this.txtUserCount.Location = new System.Drawing.Point(135, 68);
            this.txtUserCount.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtUserCount.Name = "txtUserCount";
            this.txtUserCount.Size = new System.Drawing.Size(110, 26);
            this.txtUserCount.TabIndex = 42;
            this.txtUserCount.Text = "0";
            // 
            // grouper3
            // 
            this.grouper3.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(228)))), ((int)(((byte)(242)))));
            this.grouper3.BackgroundGradientColor = System.Drawing.Color.White;
            this.grouper3.BackgroundGradientMode = GroupBoxControl.Grouper.GroupBoxGradientMode.None;
            this.grouper3.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(180)))), ((int)(((byte)(209)))));
            this.grouper3.BorderThickness = 1F;
            this.grouper3.Controls.Add(this.grouper5);
            this.grouper3.Controls.Add(this.grouper4);
            this.grouper3.CustomGroupBoxColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(180)))), ((int)(((byte)(209)))));
            this.grouper3.GroupImage = null;
            this.grouper3.GroupTitle = "";
            this.grouper3.Location = new System.Drawing.Point(15, 210);
            this.grouper3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.grouper3.Name = "grouper3";
            this.grouper3.Padding = new System.Windows.Forms.Padding(30, 31, 30, 31);
            this.grouper3.PaintGroupBox = false;
            this.grouper3.RoundCorners = 4;
            this.grouper3.ShadowColor = System.Drawing.Color.DarkGray;
            this.grouper3.ShadowControl = false;
            this.grouper3.ShadowThickness = 1;
            this.grouper3.Size = new System.Drawing.Size(496, 259);
            this.grouper3.TabIndex = 12;
            // 
            // grouper5
            // 
            this.grouper5.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(228)))), ((int)(((byte)(242)))));
            this.grouper5.BackgroundGradientColor = System.Drawing.Color.White;
            this.grouper5.BackgroundGradientMode = GroupBoxControl.Grouper.GroupBoxGradientMode.None;
            this.grouper5.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(180)))), ((int)(((byte)(209)))));
            this.grouper5.BorderThickness = 1F;
            this.grouper5.Controls.Add(this.label9);
            this.grouper5.Controls.Add(this.label8);
            this.grouper5.Controls.Add(this.txtBISTSirketleriPYS);
            this.grouper5.Controls.Add(this.txtCompaniesPeriod);
            this.grouper5.Controls.Add(this.label7);
            this.grouper5.Controls.Add(this.label6);
            this.grouper5.Controls.Add(this.txtCompanyLink);
            this.grouper5.Controls.Add(this.txtBISTSirketleriYK);
            this.grouper5.CustomGroupBoxColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(180)))), ((int)(((byte)(209)))));
            this.grouper5.GroupImage = null;
            this.grouper5.GroupTitle = "Companies";
            this.grouper5.Location = new System.Drawing.Point(10, 100);
            this.grouper5.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.grouper5.Name = "grouper5";
            this.grouper5.Padding = new System.Windows.Forms.Padding(30, 31, 30, 31);
            this.grouper5.PaintGroupBox = false;
            this.grouper5.RoundCorners = 4;
            this.grouper5.ShadowColor = System.Drawing.Color.DarkGray;
            this.grouper5.ShadowControl = false;
            this.grouper5.ShadowThickness = 1;
            this.grouper5.Size = new System.Drawing.Size(472, 153);
            this.grouper5.TabIndex = 44;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label9.Location = new System.Drawing.Point(8, 94);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(157, 22);
            this.label9.TabIndex = 54;
            this.label9.Text = "BIST Şirketleri (PYŞ)";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label8.Location = new System.Drawing.Point(9, 65);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(148, 22);
            this.label8.TabIndex = 53;
            this.label8.Text = "BIST Şirketleri (YK)";
            // 
            // txtBISTSirketleriPYS
            // 
            this.txtBISTSirketleriPYS.Location = new System.Drawing.Point(177, 94);
            this.txtBISTSirketleriPYS.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtBISTSirketleriPYS.Name = "txtBISTSirketleriPYS";
            this.txtBISTSirketleriPYS.Size = new System.Drawing.Size(275, 26);
            this.txtBISTSirketleriPYS.TabIndex = 52;
            this.txtBISTSirketleriPYS.Text = "https://www.kap.org.tr/tr/sirketler/PYS";
            // 
            // txtCompaniesPeriod
            // 
            this.txtCompaniesPeriod.Location = new System.Drawing.Point(177, 122);
            this.txtCompaniesPeriod.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtCompaniesPeriod.Name = "txtCompaniesPeriod";
            this.txtCompaniesPeriod.Size = new System.Drawing.Size(78, 26);
            this.txtCompaniesPeriod.TabIndex = 51;
            this.txtCompaniesPeriod.Text = "1440";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label7.Location = new System.Drawing.Point(9, 124);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(109, 22);
            this.label7.TabIndex = 50;
            this.label7.Text = "Period: (min)";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label6.Location = new System.Drawing.Point(8, 36);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(113, 22);
            this.label6.TabIndex = 49;
            this.label6.Text = "BIST Şirketleri";
            // 
            // txtCompanyLink
            // 
            this.txtCompanyLink.Location = new System.Drawing.Point(177, 36);
            this.txtCompanyLink.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtCompanyLink.Name = "txtCompanyLink";
            this.txtCompanyLink.Size = new System.Drawing.Size(275, 26);
            this.txtCompanyLink.TabIndex = 48;
            this.txtCompanyLink.Text = "https://www.kap.org.tr/tr/bist-sirketler";
            // 
            // txtBISTSirketleriYK
            // 
            this.txtBISTSirketleriYK.Location = new System.Drawing.Point(177, 65);
            this.txtBISTSirketleriYK.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtBISTSirketleriYK.Name = "txtBISTSirketleriYK";
            this.txtBISTSirketleriYK.Size = new System.Drawing.Size(275, 26);
            this.txtBISTSirketleriYK.TabIndex = 42;
            this.txtBISTSirketleriYK.Text = "https://www.kap.org.tr/tr/sirketler/YK";
            // 
            // grouper4
            // 
            this.grouper4.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(228)))), ((int)(((byte)(242)))));
            this.grouper4.BackgroundGradientColor = System.Drawing.Color.White;
            this.grouper4.BackgroundGradientMode = GroupBoxControl.Grouper.GroupBoxGradientMode.None;
            this.grouper4.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(180)))), ((int)(((byte)(209)))));
            this.grouper4.BorderThickness = 1F;
            this.grouper4.Controls.Add(this.txtTodayToHistoryPeriod);
            this.grouper4.Controls.Add(this.label5);
            this.grouper4.Controls.Add(this.textBox1);
            this.grouper4.CustomGroupBoxColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(180)))), ((int)(((byte)(209)))));
            this.grouper4.GroupImage = null;
            this.grouper4.GroupTitle = "TodayToHistory";
            this.grouper4.Location = new System.Drawing.Point(10, 13);
            this.grouper4.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.grouper4.Name = "grouper4";
            this.grouper4.Padding = new System.Windows.Forms.Padding(30, 31, 30, 31);
            this.grouper4.PaintGroupBox = false;
            this.grouper4.RoundCorners = 4;
            this.grouper4.ShadowColor = System.Drawing.Color.DarkGray;
            this.grouper4.ShadowControl = false;
            this.grouper4.ShadowThickness = 1;
            this.grouper4.Size = new System.Drawing.Size(216, 77);
            this.grouper4.TabIndex = 43;
            // 
            // txtTodayToHistoryPeriod
            // 
            this.txtTodayToHistoryPeriod.Location = new System.Drawing.Point(126, 34);
            this.txtTodayToHistoryPeriod.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtTodayToHistoryPeriod.Name = "txtTodayToHistoryPeriod";
            this.txtTodayToHistoryPeriod.Size = new System.Drawing.Size(78, 26);
            this.txtTodayToHistoryPeriod.TabIndex = 49;
            this.txtTodayToHistoryPeriod.Text = "10";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label5.Location = new System.Drawing.Point(9, 36);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(109, 22);
            this.label5.TabIndex = 48;
            this.label5.Text = "Period: (min)";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(131, 125);
            this.textBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(331, 26);
            this.textBox1.TabIndex = 42;
            this.textBox1.Text = "https://kap-pro.firebaseio.com/";
            // 
            // grouper2
            // 
            this.grouper2.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(228)))), ((int)(((byte)(242)))));
            this.grouper2.BackgroundGradientColor = System.Drawing.Color.White;
            this.grouper2.BackgroundGradientMode = GroupBoxControl.Grouper.GroupBoxGradientMode.None;
            this.grouper2.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(180)))), ((int)(((byte)(209)))));
            this.grouper2.BorderThickness = 1F;
            this.grouper2.Controls.Add(this.label3);
            this.grouper2.Controls.Add(this.txtKapBaseLink);
            this.grouper2.Controls.Add(this.label1);
            this.grouper2.Controls.Add(this.txtKapLink);
            this.grouper2.Controls.Add(this.label2);
            this.grouper2.Controls.Add(this.txtLastNotId);
            this.grouper2.CustomGroupBoxColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(180)))), ((int)(((byte)(209)))));
            this.grouper2.GroupImage = null;
            this.grouper2.GroupTitle = "KAP Parameters";
            this.grouper2.Location = new System.Drawing.Point(519, 36);
            this.grouper2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.grouper2.Name = "grouper2";
            this.grouper2.Padding = new System.Windows.Forms.Padding(30, 31, 30, 31);
            this.grouper2.PaintGroupBox = false;
            this.grouper2.RoundCorners = 4;
            this.grouper2.ShadowColor = System.Drawing.Color.DarkGray;
            this.grouper2.ShadowControl = false;
            this.grouper2.ShadowThickness = 1;
            this.grouper2.Size = new System.Drawing.Size(496, 138);
            this.grouper2.TabIndex = 11;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label3.Location = new System.Drawing.Point(18, 38);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 22);
            this.label3.TabIndex = 49;
            this.label3.Text = "Base Link";
            // 
            // txtKapBaseLink
            // 
            this.txtKapBaseLink.Location = new System.Drawing.Point(106, 36);
            this.txtKapBaseLink.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtKapBaseLink.Name = "txtKapBaseLink";
            this.txtKapBaseLink.Size = new System.Drawing.Size(376, 26);
            this.txtKapBaseLink.TabIndex = 48;
            this.txtKapBaseLink.Text = "https://www.kap.org.tr/tr/";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label1.Location = new System.Drawing.Point(18, 70);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 22);
            this.label1.TabIndex = 47;
            this.label1.Text = "Parse Link";
            // 
            // txtKapLink
            // 
            this.txtKapLink.Location = new System.Drawing.Point(106, 68);
            this.txtKapLink.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtKapLink.Name = "txtKapLink";
            this.txtKapLink.Size = new System.Drawing.Size(376, 26);
            this.txtKapLink.TabIndex = 46;
            this.txtKapLink.Text = "https://www.kap.org.tr/tr/Bildirim/";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label2.Location = new System.Drawing.Point(18, 103);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 22);
            this.label2.TabIndex = 45;
            this.label2.Text = "Last ID";
            // 
            // txtLastNotId
            // 
            this.txtLastNotId.Location = new System.Drawing.Point(106, 99);
            this.txtLastNotId.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtLastNotId.Name = "txtLastNotId";
            this.txtLastNotId.Size = new System.Drawing.Size(376, 26);
            this.txtLastNotId.TabIndex = 42;
            this.txtLastNotId.Text = "100000";
            // 
            // btnStop
            // 
            this.btnStop.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btnStop.Enabled = false;
            this.btnStop.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(180)))), ((int)(((byte)(209)))));
            this.btnStop.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(180)))), ((int)(((byte)(209)))));
            this.btnStop.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(180)))), ((int)(((byte)(209)))));
            this.btnStop.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStop.Font = new System.Drawing.Font("Segoe UI Semibold", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnStop.Location = new System.Drawing.Point(861, 477);
            this.btnStop.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(118, 42);
            this.btnStop.TabIndex = 10;
            this.btnStop.Tag = "ApplicationMessageRequest";
            this.btnStop.Text = "Stop";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // headerPanel1
            // 
            this.headerPanel1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.headerPanel1.Controls.Add(this.grpMessageType);
            this.headerPanel1.Controls.Add(this.txtSecurityCode);
            this.headerPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.headerPanel1.HeaderColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(205)))), ((int)(((byte)(219)))));
            this.headerPanel1.HeaderColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(180)))), ((int)(((byte)(209)))));
            this.headerPanel1.HeaderFont = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.headerPanel1.HeaderHeight = 24;
            this.headerPanel1.HeaderText = "Parameters";
            this.headerPanel1.Icon = null;
            this.headerPanel1.IconTransparentColor = System.Drawing.Color.White;
            this.headerPanel1.Location = new System.Drawing.Point(0, 33);
            this.headerPanel1.Margin = new System.Windows.Forms.Padding(8, 6, 8, 6);
            this.headerPanel1.Name = "headerPanel1";
            this.headerPanel1.Padding = new System.Windows.Forms.Padding(8, 43, 8, 6);
            this.headerPanel1.Size = new System.Drawing.Size(1053, 583);
            this.headerPanel1.TabIndex = 30;
            // 
            // txtSecurityCode
            // 
            this.txtSecurityCode.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtSecurityCode.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtSecurityCode.Location = new System.Drawing.Point(14, 125);
            this.txtSecurityCode.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtSecurityCode.Name = "txtSecurityCode";
            this.txtSecurityCode.Size = new System.Drawing.Size(170, 30);
            this.txtSecurityCode.TabIndex = 1;
            this.txtSecurityCode.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtSecurityCode.Visible = false;
            // 
            // grouper7
            // 
            this.grouper7.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(228)))), ((int)(((byte)(242)))));
            this.grouper7.BackgroundGradientColor = System.Drawing.Color.White;
            this.grouper7.BackgroundGradientMode = GroupBoxControl.Grouper.GroupBoxGradientMode.None;
            this.grouper7.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(180)))), ((int)(((byte)(209)))));
            this.grouper7.BorderThickness = 1F;
            this.grouper7.Controls.Add(this.label12);
            this.grouper7.Controls.Add(this.txtOSAppID);
            this.grouper7.Controls.Add(this.label15);
            this.grouper7.Controls.Add(this.txtOSRestApiKey);
            this.grouper7.Controls.Add(this.label16);
            this.grouper7.CustomGroupBoxColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(180)))), ((int)(((byte)(209)))));
            this.grouper7.GroupImage = null;
            this.grouper7.GroupTitle = "Notification Parameters";
            this.grouper7.Location = new System.Drawing.Point(519, 292);
            this.grouper7.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.grouper7.Name = "grouper7";
            this.grouper7.Padding = new System.Windows.Forms.Padding(30, 31, 30, 31);
            this.grouper7.PaintGroupBox = false;
            this.grouper7.RoundCorners = 4;
            this.grouper7.ShadowColor = System.Drawing.Color.DarkGray;
            this.grouper7.ShadowControl = false;
            this.grouper7.ShadowThickness = 1;
            this.grouper7.Size = new System.Drawing.Size(496, 105);
            this.grouper7.TabIndex = 14;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label12.Location = new System.Drawing.Point(18, 38);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(133, 22);
            this.label12.TabIndex = 49;
            this.label12.Text = "OneSignalAppID";
            // 
            // txtOSAppID
            // 
            this.txtOSAppID.Location = new System.Drawing.Point(153, 36);
            this.txtOSAppID.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtOSAppID.Name = "txtOSAppID";
            this.txtOSAppID.Size = new System.Drawing.Size(329, 26);
            this.txtOSAppID.TabIndex = 48;
            this.txtOSAppID.Text = "55161f39-9ba9-4677-bcb1-e5b22b364891";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label15.Location = new System.Drawing.Point(18, 70);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(96, 22);
            this.label15.TabIndex = 47;
            this.label15.Text = "RestAPIKey";
            // 
            // txtOSRestApiKey
            // 
            this.txtOSRestApiKey.Location = new System.Drawing.Point(153, 68);
            this.txtOSRestApiKey.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtOSRestApiKey.Name = "txtOSRestApiKey";
            this.txtOSRestApiKey.Size = new System.Drawing.Size(329, 26);
            this.txtOSRestApiKey.TabIndex = 46;
            this.txtOSRestApiKey.Text = "N2U4OThhYTctZmY2NC00ZjdmLTg2MjEtYWVmYzM0MTFmYzgw";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label16.Location = new System.Drawing.Point(18, 103);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(0, 22);
            this.label16.TabIndex = 45;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1053, 877);
            this.Controls.Add(this.header);
            this.Controls.Add(this.headerPanel1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frmMain";
            this.Text = "KAP Parser";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMain_FormClosing);
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.header.ResumeLayout(false);
            this.grouper1.ResumeLayout(false);
            this.grouper1.PerformLayout();
            this.grpMessageType.ResumeLayout(false);
            this.grouper6.ResumeLayout(false);
            this.grouper6.PerformLayout();
            this.grouper3.ResumeLayout(false);
            this.grouper5.ResumeLayout(false);
            this.grouper5.PerformLayout();
            this.grouper4.ResumeLayout(false);
            this.grouper4.PerformLayout();
            this.grouper2.ResumeLayout(false);
            this.grouper2.PerformLayout();
            this.headerPanel1.ResumeLayout(false);
            this.headerPanel1.PerformLayout();
            this.grouper7.ResumeLayout(false);
            this.grouper7.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem configToolStripMenuItem;
        private Controls.HeaderPanel header;
        private System.Windows.Forms.ListBox lstLog;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.TextBox txtFireBasePath;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtAuthKey;
        private System.Windows.Forms.Label label14;
        private GroupBoxControl.Grouper grouper1;
        private GroupBoxControl.Grouper grpMessageType;
        private System.Windows.Forms.Button btnStop;
        private Controls.HeaderPanel headerPanel1;
        private System.Windows.Forms.TextBox txtSecurityCode;
        private GroupBoxControl.Grouper grouper2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtKapLink;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtLastNotId;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtKapBaseLink;
        private GroupBoxControl.Grouper grouper3;
        private GroupBoxControl.Grouper grouper4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox1;
        private GroupBoxControl.Grouper grouper5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtCompanyLink;
        private System.Windows.Forms.TextBox txtBISTSirketleriYK;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtCompaniesPeriod;
        private System.Windows.Forms.TextBox txtTodayToHistoryPeriod;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtBISTSirketleriPYS;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtNotificationKey;
        private GroupBoxControl.Grouper grouper6;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtUserCount;
        private System.Windows.Forms.TextBox txtUserPeriod;
        private System.Windows.Forms.Label label11;
        private GroupBoxControl.Grouper grouper7;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtOSAppID;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtOSRestApiKey;
        private System.Windows.Forms.Label label16;
    }
}

