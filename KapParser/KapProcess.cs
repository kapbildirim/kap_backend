﻿using log4net;
using ParserManager;
using ProcessManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FireManager;
using Cache;
using System.Threading;
using LogProvider;

namespace KapMaster
{
    class KapProcess : IProcess
    {
        private Task processTask;
        private volatile bool isPaused = false;
        private volatile bool isReady = false;
        public bool Pause { get => isPaused; set => isPaused = value; }
        public bool Ready { get => isReady; set => isReady = value; }
        ILog log = LogManager.GetLogger(typeof(KapProcess));
        IParser<IModel> parser;
        private int kapInitialNumber;
        private string kapLink;
        private IDBase firebase;
        private Tasks task;
        public event Action<string, ELogType> OnLogEvent;

        private static KapProcess kapProcess;
        private static object islocked = new object();

        public static KapProcess GetInstance(string kapLink, int kapInitialNumber)
        {
            if (kapProcess == null)
            {
                lock (islocked)
                {
                    if (kapProcess == null)
                    {
                        kapProcess = new KapProcess(kapLink, kapInitialNumber);
                    }
                }
            }

            return kapProcess;
        }

        public static KapProcess GetInstance()
        {
            if (kapProcess == null)
            {
                lock (islocked)
                {
                    if (kapProcess == null)
                    {
                        kapProcess = new KapProcess("", 0);
                    }
                }
            }

            return kapProcess;
        }

        private KapProcess(string kapLink,int kapInitialNumber)
        {
            firebase = Provider.Execute();
            parser = ParserFactory<KapParser>.GetParser();
            this.kapInitialNumber = kapInitialNumber;
            this.kapLink = kapLink;
            task = Tasks.GetInstance();
            isReady = false;
        }

        public bool Start()
        {
            try
            {
                if ((processTask == null) || (processTask.Status != TaskStatus.Running))
                {
                    processTask = Task.Factory.StartNew(new Action(() =>
                    {
                        log.Info("KAP Process Started: " + DateTime.Now);
                        ScreenLog(kapInitialNumber.ToString(), ELogType.Start);
                        ScreenLog("KAP Process Started", ELogType.Information);
                        try
                        {
                            while (!isPaused)
                            {
                                try
                                {
                                    if (!Ready) //Şirket Bilgileri hazır olduktan sonra bildirimleri işlemeli.
                                    {
                                        Thread.Sleep(1000);
                                        continue;
                                    }

                                    KapModel model = (KapModel)parser.Execute(kapLink, kapInitialNumber);

                                    if (model == null)
                                    {
                                        ScreenLog(string.Format("Model Not Found {0}, {1}", kapInitialNumber, kapLink), ELogType.Information);
                                        Thread.Sleep(5000);
                                        continue;
                                    }
                                    List<int> indicesIds;
                                    CategoryProcess.GetInstance().CompanyToCategories.TryGetValue(model.ShortName, out indicesIds);
                                    model.Indices = indicesIds;
                                    DateTime date = ConvertHelper.ConvertValue<DateTime>(model.Date);
                                    string key = (DateTime.Now.Date - date.Date).TotalDays != 0 ? KapTable.History.ToString() + "/" + date.ToString("yyyyMMdd") : KapTable.Today.ToString();
                                    key = string.Concat(key, "/", model.Id);
                                    model.Timestamp = long.Parse(DateTime.Now.ToString("yyyyMMddHHmmssfff"));
                                    firebase.Execute<IModel>(key, model);
                                    FileCache.Insert<object>(Parameters.KapLastIdKey, (++kapInitialNumber).ToString());
                                    NotificationProcess.GetInstance("").Process(model);
                                    ScreenLog(kapInitialNumber.ToString(), ELogType.KapLastIdKey);
                                }
                                catch (Exception ex)
                                {
                                    if (ex.HResult == -2146233079)
                                    {
                                        Thread.Sleep(5000);
                                        continue;
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            ScreenLog(ex.ToString(), ELogType.Information);
                        }
                        ScreenLog(kapInitialNumber.ToString(), ELogType.Stop);
                        ScreenLog("KAP Process Ended", ELogType.Information);
                    }));
                }
                else
                {
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                ScreenLog(kapInitialNumber.ToString(), ELogType.Stop);
                log.Error(ex);
                processTask = null;
                return false;
            }
        }

        public bool Stop()
        {
            Pause = true;
            return Pause;
        }

        private void ScreenLog(string message, ELogType process)
        {
            if (OnLogEvent != null)
                OnLogEvent(message, process);
        }
    }
}
