﻿using ProcessManager;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FireManager;
using LogProvider;
using log4net;
using ParserManager;
using System.Threading;

namespace KapMaster
{
    class TodayToHistoryProcess : IProcess
    {
        public bool Pause { get; set; }
        private IDBase firebase;
        private Task processTask;
        ILog log = LogManager.GetLogger(typeof(TodayToHistoryProcess));
        public event Action<string, ELogType> OnLogEvent;
        private Timer timer;
        TimeSpan startTimeSpan = TimeSpan.Zero;
        TimeSpan periodTimeSpan = TimeSpan.FromMinutes(5);

        private static TodayToHistoryProcess todayToHistoryProcess;
        private static object islocked = new object();

        public static TodayToHistoryProcess GetInstance(int period)
        {
            if (todayToHistoryProcess == null)
            {
                lock (islocked)
                {
                    if (todayToHistoryProcess == null)
                    {
                        todayToHistoryProcess = new TodayToHistoryProcess(period);
                    }
                }
            }

            return todayToHistoryProcess;
        }

        private TodayToHistoryProcess(int period)
        {
            firebase = Provider.Execute();
            periodTimeSpan = TimeSpan.FromMinutes(period);
        }

        public bool Start()
        {
            if (timer == null)
            {
                timer = new Timer((e) =>
                {
                    Process();
                }, null, startTimeSpan, periodTimeSpan);
            }
            
            return true;
        }

        public bool Process()
        {
            try
            {
                if ((processTask == null) || (processTask.Status != TaskStatus.Running))
                {
                    processTask = Task.Factory.StartNew(new Action(() =>
                    {
                        log.Info("Today To History Process Started: " + DateTime.Now);
                        ScreenLog("Today To History Process Started", ELogType.Information);
                        try
                        {
                            Dictionary<string,KapModel> todayModels = firebase.Get<KapModel>(KapTable.Today.ToString());
                            if (todayModels != null && todayModels.Count > 0)
                            {
                                string key = "";
                                foreach (var model in todayModels.Values)
                                {
                                    DateTime date = ConvertHelper.ConvertValue<DateTime>(model.Date);
                                    if ((DateTime.Now.Date - date.Date).TotalDays != 0)
                                    {
                                        key = KapTable.History.ToString() + "/" + date.ToString("yyyyMMdd") + "/" + model.Id;
                                        model.Timestamp = long.Parse(DateTime.Now.ToString("yyyyMMddHHmmssfff"));
                                        firebase.Execute<IModel>(key, model);
                                        key = KapTable.Today.ToString() + "/" + model.Id;
                                        model.IsDelete = true;
                                        firebase.Execute<IModel>(key, model);
                                        firebase.Delete<IModel>(key);
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            ScreenLog(ex.ToString(), ELogType.Information);
                            log.Error(ex);
                        }
                    }));
                }
                else
                {
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                ScreenLog(ex.ToString(), ELogType.Stop);
                log.Error(ex);
                processTask = null;
                return false;
            }
        }

        public bool Stop()
        {
            try
            {
                Pause = true;
                timer.Change(Timeout.Infinite, Timeout.Infinite);
                timer = null;
                return Pause;
            }
            catch
            {
                return false;
            }
        }

        private void ScreenLog(string message, ELogType process)
        {
            OnLogEvent?.Invoke(message, process);
        }
    }
}
