﻿using ProcessManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FireManager;
using LogProvider;
using log4net;
using ParserManager;
using System.Threading;
using ParserManager.Parser;
using System.Collections.Concurrent;
using ParserManager.Model;

namespace KapMaster
{
    class CategoryProcess : IProcess
    {
        public bool Pause { get; set; }
        private IDBase firebase;
        private Task processTask;
        ILog log = LogManager.GetLogger(typeof(CategoryProcess));
        public event Action<string, ELogType> OnLogEvent;

        private static CategoryProcess categoryProcess;
        private static object islocked = new object();
        public static CategoryProcess GetInstance()
        {
            if (categoryProcess == null)
            {
                lock (islocked)
                {
                    if (categoryProcess == null)
                    {
                        categoryProcess = new CategoryProcess();
                    }
                }
            }

            return categoryProcess;
        }
        public SortedDictionary<string, CategoryModel> Categories { get; set; }
        public Dictionary<string, List<int>> CompanyToCategories { get; set; }

        private CategoryProcess()
        {
            firebase = Provider.Execute();
            Categories = new SortedDictionary<string, CategoryModel>();
            CompanyToCategories = new Dictionary<string, List<int>>();
        }

        public bool Start()
        {
            Process();
            return true;
        }

        private List<int> GetIndecesIds(string[] indeces)
        {
            try
            {
                List<int> indxes = new List<int>();
                CategoryModel value;
                for (int i = 0; i < indeces.Length; ++i)
                {
                    Categories.TryGetValue(indeces[i].Trim(), out value);
                    indxes.Add(value.Id);
                }
                return indxes;
            }
            catch (Exception ex)
            {
                ScreenLog(ex.ToString(), ELogType.Information);
                return null;
            }
        }

        public bool Process()
        {
            try
            {
                try
                {
                    ScreenLog("Category Process Started", ELogType.Information);
                    Categories = new SortedDictionary<string, CategoryModel>();
                    var companyList = firebase.Get<CompanyModel>(KapTable.Company.ToString());
                    var categoryFirebase = firebase.Get<CategoryModel>(KapTable.Category.ToString());
                    if (companyList == null) return false;
                    var sortedCompanyList =  new SortedDictionary<string, CompanyModel>(companyList);
                   
                    foreach (var kvp in sortedCompanyList)
                    {
                        if (kvp.Value.CompanyType == 1)
                        {
                            var indeces = kvp.Value.DetailModel.BISTIndices.Split('/');
                            for (int i = 0; i < indeces.Length; ++i)
                            {
                                if (Categories.TryGetValue(indeces[i].Trim(), out CategoryModel a))
                                    continue;
                                if (indeces[i].Trim().Length <= 0)
                                    continue;

                                CategoryModel model = new CategoryModel();
                                model.IsVisible = (indeces[i].Trim() == "BIST 30" || indeces[i].Trim() == "BIST 50" || indeces[i].Trim() == "BIST 100") ? true : false;
                                model.Id = Categories.Count() + 1;
                                model.Name = indeces[i].Trim();
                                Categories.Add(model.Name, model);
                            }

                            if (indeces[0].Trim().Length <= 0)
                                continue;

                            CompanyToCategories.Add(kvp.Key, GetIndecesIds(indeces));
                        }
                    }

                    if (categoryFirebase != null)
                    {
                        List<CategoryModel> firebaseValues = categoryFirebase.Values.ToList();
                        List<CategoryModel> categories = Categories.Values.ToList();

                        var newList = firebaseValues.Except(categories, new IdComparer()).ToList();
                        string key = "";
                        foreach (var model in newList)
                        {
                            key = KapTable.Category.ToString() + "/" + model.Name;
                            model.IsDelete = true;
                            model.Timestamp = long.Parse(DateTime.Now.ToString("yyyyMMddHHmmssfff"));
                            firebase.Execute<CategoryModel>(key, model);
                            Thread.Sleep(1);
                            firebase.Delete<CategoryModel>(key);
                        }


                        firebaseValues = categoryFirebase.Values.ToList();
                        newList = categories.Except(firebaseValues, new IdComparer()).ToList();

                        foreach (var category in newList)
                        {
                            key = KapTable.Category.ToString() + "/" + category.Name.Replace('.', ' ');
                            category.Timestamp = long.Parse(DateTime.Now.ToString("yyyyMMddHHmmssfff"));
                            firebase.Execute<CategoryModel>(key, category);
                        }
                    }
                    else
                    {
                        string key = "";
                        foreach (var category in Categories)
                        {
                            key = KapTable.Category.ToString() + "/" + category.Key.Replace('.', ' ');
                            category.Value.Timestamp = long.Parse(DateTime.Now.ToString("yyyyMMddHHmmssfff"));
                            firebase.Execute<CategoryModel>(key, category.Value);
                        }
                    }
                }
                catch (Exception ex)
                {
                    ScreenLog(ex.ToString(), ELogType.Information);
                    log.Error(ex);
                }
                ScreenLog("Category Process Completed", ELogType.Information);
                return true;
            }
            catch (Exception ex)
            {
                ScreenLog(typeof(CompanyProcess).ToString() + " " + ex.ToString(), ELogType.Stop);
                log.Error(ex);
                processTask = null;
                return false;
            }
        }

        public bool Stop()
        {
            try
            {
                Pause = true;
                return Pause;
            }
            catch
            {
                return false;
            }
        }

        private void ScreenLog(string message, ELogType process)
        {
            OnLogEvent?.Invoke(message, process);
        }

        public class IdComparer : IEqualityComparer<CategoryModel>
        {
            public int GetHashCode(CategoryModel co)
            {
                if (co == null)
                {
                    return 0;
                }
                return co.Id.GetHashCode();
            }

            public bool Equals(CategoryModel x1, CategoryModel x2)
            {
                if (ReferenceEquals(x1, x2))
                {
                    return true;
                }
                if (ReferenceEquals(x1, null) ||
                    ReferenceEquals(x2, null))
                {
                    return false;
                }
                return (x1.Id == x2.Id &&
                        x1.IsDelete == x2.IsDelete &&
                        x1.IsVisible == x2.IsVisible);
            }
        }
    }
}
