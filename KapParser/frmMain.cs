﻿using System;
using System.Windows.Forms;
using Cache;
using log4net;
using ProcessManager;
using LogProvider;

namespace KapMaster
{
    public partial class frmMain : Form
    {
        private KapProcess kapProcess;
        private TodayToHistoryProcess todayToHistoryProcess;
        private CompanyProcess companyProcess;
        private UserProcess userProcess;
        private NotificationProcess notificationProcess;
        ILog log = LogManager.GetLogger(typeof(frmMain));

        public frmMain()
        {
            InitializeComponent();
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            try
            {
                SaveParameters();
                btnStart.Enabled = false;
                int value;
                if (int.TryParse(txtLastNotId.Text, out value))
                {
                    kapProcess = KapProcess.GetInstance(txtKapLink.Text, value);
                    kapProcess.OnLogEvent -= Process_OnLogEvent;
                    kapProcess.OnLogEvent += Process_OnLogEvent;
                    kapProcess.Start();
                }

                if (int.TryParse(txtTodayToHistoryPeriod.Text, out value))
                {
                    todayToHistoryProcess = TodayToHistoryProcess.GetInstance(value);
                    todayToHistoryProcess.OnLogEvent -= Process_OnLogEvent;
                    todayToHistoryProcess.OnLogEvent += Process_OnLogEvent;
                    todayToHistoryProcess.Start();
                }

                if (int.TryParse(txtCompaniesPeriod.Text, out value))
                {
                    companyProcess = CompanyProcess.GetInstance(txtCompanyLink.Text, txtBISTSirketleriYK.Text, txtBISTSirketleriPYS.Text, value);
                    companyProcess.OnLogEvent -= Process_OnLogEvent;
                    companyProcess.OnLogEvent += Process_OnLogEvent;
                    companyProcess.Start();
                }

                if (int.TryParse(txtUserPeriod.Text, out value))
                {
                    userProcess = UserProcess.GetInstance(value);
                    userProcess.OnLogEvent -= Process_OnLogEvent;
                    userProcess.OnLogEvent += Process_OnLogEvent;
                    userProcess.Start();
                }


                notificationProcess = NotificationProcess.GetInstance(txtOSRestApiKey.Text);
                notificationProcess.OnLogEvent -= Process_OnLogEvent;
                notificationProcess.OnLogEvent += Process_OnLogEvent;
                notificationProcess.OSAppID = txtOSAppID.Text;
                notificationProcess.Start();

            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString());
            }
        }

        private void Process_OnLogEvent(string message, ELogType type)
        {
            try
            {
                if (type == ELogType.Information)
                    WriteLog(message);
                else if (type == ELogType.Start)
                {
                    btnStart.BeginInvoke(new Action(() => { btnStart.Enabled = false; }));
                    btnStop.BeginInvoke(new Action(() => { btnStop.Enabled = true; }));
                }
                else if (type == ELogType.Stop)
                {
                    btnStart.BeginInvoke(new Action(() => { btnStart.Enabled = true; }));
                    btnStop.BeginInvoke(new Action(() => { btnStop.Enabled = false; }));
                }
                else if (type == ELogType.KapLastIdKey)
                {
                    txtLastNotId.BeginInvoke(new Action(() => { txtLastNotId.Text = message; }));
                }
                else if (type == ELogType.UserCount)
                {
                    txtUserCount.BeginInvoke(new Action(() => { txtUserCount.Text = message; }));
                }
            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString());
            }
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            kapProcess.Stop();
            todayToHistoryProcess.Stop();
            companyProcess.Stop();
            userProcess.Stop();
            notificationProcess.Stop();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            GetParameters();
        }

        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (kapProcess != null)
                kapProcess.Stop();
            if (todayToHistoryProcess != null)
                todayToHistoryProcess.Stop();
            if (companyProcess != null)
                companyProcess.Stop();
            if (userProcess != null)
                userProcess.Stop();
            if (notificationProcess != null)
                notificationProcess.Stop();
            SaveParameters();
        }

        private void SaveParameters()
        {
            try
            {
                FileCache.Insert<object>(Parameters.KapBaseLinkKey, txtKapBaseLink.Text);
                FileCache.Insert<object>(Parameters.KapLinkKey, txtKapLink.Text);
                FileCache.Insert<object>(Parameters.KapLastIdKey, txtLastNotId.Text);
                FileCache.Insert<object>(Parameters.FireBasePathKey, txtFireBasePath.Text);
                FileCache.Insert<object>(Parameters.FireBaseAuthKey, txtAuthKey.Text);
                FileCache.Insert<object>(Parameters.CompanyLinkKey, txtCompanyLink.Text);
                FileCache.Insert<object>(Parameters.CompaniesPeriod, txtCompaniesPeriod.Text);
                FileCache.Insert<object>(Parameters.TodayToHistoryPeriod, txtTodayToHistoryPeriod.Text);
                FileCache.Insert<object>(Parameters.BISTSirketleriYK, txtBISTSirketleriYK.Text);
                FileCache.Insert<object>(Parameters.BISTSirketleriPYS, txtBISTSirketleriPYS.Text);
                FileCache.Insert<object>(Parameters.NotificationKey, txtNotificationKey.Text);
                FileCache.Insert<object>(Parameters.UserProcessPeriod, txtUserPeriod.Text);
                FileCache.Insert<object>(Parameters.OSAppID, txtOSAppID.Text);
                FileCache.Insert<object>(Parameters.OSRestApiKey, txtOSRestApiKey.Text);
            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString());
            }
        }

        private void FillParameter(string key, object o)
        {
            try
            {
                TextBox box = o as TextBox;
                object p = FileCache.GetOrAdd<object>(key, box.Text);
                if (!p.Equals(null)) box.Text = (string)p;
            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString());
            }
        }

        private void GetParameters()
        {
            FillParameter(Parameters.KapBaseLinkKey, txtKapBaseLink);
            FillParameter(Parameters.KapLinkKey, txtKapLink);
            FillParameter(Parameters.KapLastIdKey, txtLastNotId);
            FillParameter(Parameters.FireBasePathKey, txtFireBasePath);
            FillParameter(Parameters.FireBaseAuthKey, txtAuthKey);
            FillParameter(Parameters.CompanyLinkKey, txtCompanyLink);
            FillParameter(Parameters.CompaniesPeriod, txtCompaniesPeriod);
            FillParameter(Parameters.TodayToHistoryPeriod, txtTodayToHistoryPeriod);
            FillParameter(Parameters.BISTSirketleriYK, txtBISTSirketleriYK);
            FillParameter(Parameters.BISTSirketleriPYS, txtBISTSirketleriPYS);
            FillParameter(Parameters.NotificationKey, txtNotificationKey);
            FillParameter(Parameters.UserProcessPeriod, txtUserPeriod);
            FillParameter(Parameters.OSAppID, txtOSAppID);
            FillParameter(Parameters.OSRestApiKey, txtOSRestApiKey);
        }

        private void WriteLog(string message)
        {
            try
            {
                lstLog.BeginInvoke(new Action(() => { lstLog.Items.Insert(0, String.Format("<{0}>  {1}", DateTime.Now.TimeOfDay, message)); }));
            }
            catch(Exception exp)
            {
                log.Error(exp);
            }
        }
    }
}
