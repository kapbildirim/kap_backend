﻿using ProcessManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FireManager;
using LogProvider;
using log4net;
using ParserManager;
using System.Threading;
using System.Collections.Concurrent;
using ParserManager.Model;

namespace KapMaster
{
    class UserProcess : IProcess
    {
        public bool Pause { get; set; }
        private IDBase firebase;
        private Task processTask;
        ILog log = LogManager.GetLogger(typeof(UserProcess));
        public event Action<string, ELogType> OnLogEvent;
        private System.Threading.Timer timer;
        TimeSpan startTimeSpan = TimeSpan.Zero;
        TimeSpan periodTimeSpan = TimeSpan.FromMinutes(5);

        private static UserProcess userProcess;
        private static object islocked = new object();

        public static UserProcess GetInstance(int period)
        {
            if (userProcess == null)
            {
                lock (islocked)
                {
                    if (userProcess == null)
                    {
                        userProcess = new UserProcess(period);
                    }
                }
            }

            return userProcess;
        }

        public ConcurrentDictionary<string, UserModel> Users { get; set; }

        private UserProcess(int period)
        {
            firebase = Provider.Execute();
            periodTimeSpan = TimeSpan.FromMinutes(period);
            Users = new ConcurrentDictionary<string, UserModel>();
        }

        public bool Start()
        {
            if (timer == null)
            {
                timer = new System.Threading.Timer((e) =>
                {
                    Process();
                }, null, startTimeSpan, periodTimeSpan);
            }
            
            return true;
        }

        public bool Process()
        {
            try
            {
                if ((processTask == null) || (processTask.Status != TaskStatus.Running))
                {
                    processTask = Task.Factory.StartNew(new Action(() =>
                    {
                        ScreenLog("User Process Started", ELogType.Information);
                        try
                        {
                            Dictionary<string, UserModel> userModels = firebase.Get<UserModel>(KapTable.User.ToString());
                            if (userModels != null && userModels.Count > 0)
                            {
                                foreach (var model in userModels)
                                {
                                    Users.TryAdd(model.Key, model.Value);
                                }
                            }

                            ScreenLog(Users.Count.ToString(), ELogType.UserCount);
                        }
                        catch (Exception ex)
                        {
                            ScreenLog(ex.ToString(), ELogType.Information);
                            log.Error(ex);
                        }
                    }));
                }
                else
                {
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                ScreenLog(ex.ToString(), ELogType.Stop);
                log.Error(ex);
                processTask = null;
                return false;
            }
        }

        public bool Stop()
        {
            try
            {
                Pause = true;
                timer.Change(Timeout.Infinite, Timeout.Infinite);
                timer = null;
                return Pause;
            }
            catch
            {
                return false;
            }
        }

        private void ScreenLog(string message, ELogType process)
        {
            OnLogEvent?.Invoke(message, process);
        }
    }
}
