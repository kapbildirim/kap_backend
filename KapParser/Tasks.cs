﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FireManager;
using ParserManager;
using System.Collections.Concurrent;

namespace KapMaster
{
    class Tasks
    {
        private IDBase firebase;
        private static Tasks tasks;
        private static object islocked = new object();
        public Dictionary<string, CompanyModel> CompanyList { get; set; }

        public Tasks()
        {
            firebase = Provider.Execute();
        }

        public static Tasks GetInstance()
        {
            if (tasks == null)
            {
                lock (islocked)
                {
                    if (tasks == null)
                    {
                        tasks = new Tasks();
                    }
                }
            }

            return tasks;
        }

        public void CompanyProcessStart()
        {
            CompanyList = firebase.Get<CompanyModel>(KapTable.Company.ToString());

        }

        public void GetTodaysNotifications()
        {
            Dictionary<string, Dictionary<string, KapModel>> list = firebase.Get<Dictionary<string, KapModel>>("");
        }

        private void GetCompanies()
        {
            CompanyList = firebase.Get<CompanyModel>(KapTable.Company.ToString());
        }

        public void SetIndices(KapModel model)
        {
            CompanyModel company;
            if (CompanyList.TryGetValue(model.ShortName, out company))
            {
                string[] indices = company.DetailModel.BISTIndices.Split('|');
            }
        }

    }
}
