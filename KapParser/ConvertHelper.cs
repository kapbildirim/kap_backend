﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KapMaster
{
    public class ConvertHelper
    {
        public static T ConvertValue<T>(object configValue)
        {
            //if (configValue is IConvertible)
            //    return (T)Convert.ChangeType(configValue, typeof(T));
            //return (T)Convert.ChangeType(configValue, typeof(T), CultureInfo.CreateSpecificCulture("en-GB"));
            if (configValue == null || DBNull.Value.Equals(configValue))
                return default(T);

            var t = typeof(T);
            return (T)Convert.ChangeType(configValue, Nullable.GetUnderlyingType(t) ?? t);
        }
    }
}
