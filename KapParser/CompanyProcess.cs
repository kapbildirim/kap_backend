﻿using ProcessManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FireManager;
using LogProvider;
using log4net;
using ParserManager;
using System.Threading;
using ParserManager.Parser;
using System.Collections.Concurrent;

namespace KapMaster
{
    class CompanyProcess : IProcess
    {
        public bool Pause { get; set; }
        private IDBase firebase;
        private Task processTask;
        ILog log = LogManager.GetLogger(typeof(CompanyProcess));
        public event Action<string, ELogType> OnLogEvent;
        private System.Threading.Timer timer;
        TimeSpan startTimeSpan = TimeSpan.Zero;
        TimeSpan periodTimeSpan = TimeSpan.FromMinutes(60*24);
        IParser<List<IModel>> parser;
        IParser<List<IModel>> otherParser;

        IParser<IModel> companyDetailParser;
        IParser<IModel> otherCompanyDetailParser;

        private string url = "";
        private string urlYK = "";
        private string urlPYS = "";
        List<CompanyModel> models;
        private static CompanyProcess companyProcess;
        private static object islocked = new object();
        private CategoryProcess categoryProcess;
        public Dictionary<string, CompanyModel> CompanyList { get; set; }

        public static CompanyProcess GetInstance(string url, string urlYK, string urlPYS, int period)
        {
            if (companyProcess == null)
            {
                lock (islocked)
                {
                    if (companyProcess == null)
                    {
                        companyProcess = new CompanyProcess(url, urlYK, urlPYS, period);
                    }
                }
            }

            return companyProcess;
        }

        private CompanyProcess(string url, string urlYK, string urlPYS, int period)
        {
            firebase = Provider.Execute();
            parser = ParserFactory<KapDetailParser>.GetParser();
            otherParser = ParserFactory<OtherCompanyParser>.GetParser();
            otherCompanyDetailParser = ParserFactory<OtherCompanyDetailParser>.GetParser();
            companyDetailParser = ParserFactory<KapCompanyDetailParser>.GetParser();
            periodTimeSpan = TimeSpan.FromMinutes(period);
            categoryProcess = CategoryProcess.GetInstance();
            CompanyList = new Dictionary<string, CompanyModel>();

            this.url = url;
            this.urlYK = urlYK;
            this.urlPYS = urlPYS;
        }

        public bool Start()
        {
            if (timer == null)
            {
                timer = new System.Threading.Timer((e) =>
                {
                    Process();
                }, null, startTimeSpan, periodTimeSpan);
            }
            
            return true;
        }

        public bool Process()
        {
            try
            {
                if ((processTask == null) || (processTask.Status != TaskStatus.Running))
                {
                    processTask = Task.Factory.StartNew(new Action(() =>
                    {
                        ScreenLog("Company Process Started", ELogType.Information);
                        try
                        {
                            categoryProcess.Process();
                            CompanyList = firebase.Get<CompanyModel>(KapTable.Company.ToString());
                            if (CompanyList == null) CompanyList = new Dictionary<string, CompanyModel>();
                            var hasEmptyDetail = CompanyList.Values.Any(d => d.DetailModel == null);
                            models = parser.Execute(url, 1).ConvertAll(o => (CompanyModel)o);
                            models.AddRange(otherParser.Execute(urlYK, 2).ConvertAll(o => (CompanyModel)o));
                            models.AddRange(otherParser.Execute(urlPYS, 3).ConvertAll(o => (CompanyModel)o));
                            if (models == null)
                            {
                                ScreenLog("CompanyModel Model Not Found", ELogType.Information);
                            }
                            else
                            {
                                if (models != null && models.Count > 0)
                                {
                                    string key = "";
                                    List<CompanyModel> firebaseValues = CompanyList.Values.ToList();
                                    var newList = firebaseValues.Except(models, new IdComparer()).ToList();
                                    try
                                    {
                                        if (newList.Count == firebaseValues.Count && !hasEmptyDetail)
                                            KapProcess.GetInstance().Ready = true;
                                        //var shouldDeleteList = firebaseValues.Except(newList, new IdComparer()).ToList();
                                        foreach (var model in newList)
                                        {
                                            key = KapTable.Company.ToString() + "/" + model.Code;
                                            model.IsDelete = true;
                                            model.Timestamp = long.Parse(DateTime.Now.ToString("yyyyMMddHHmmssfff"));
                                            firebase.Execute<IModel>(key, model);
                                            Thread.Sleep(1);
                                            firebase.Delete<IModel>(key);
                                        }
                                    }
                                    catch(Exception ex)
                                    {
                                        ScreenLog(ex.ToString(), ELogType.Error);
                                    }

                                   firebaseValues = CompanyList.Values.ToList();
                                   newList = models.Except(firebaseValues, new IdComparer()).ToList();

                                    foreach (var model in newList)
                                    {
                                        var m = model as CompanyModel;
                                        if (m.CompanyType == 1)
                                            m.DetailModel = companyDetailParser.Execute(m.CompanyDetailLink, 0) as CompanyDetailModel;
                                        else
                                            m.DetailModel = otherCompanyDetailParser.Execute(m.CompanyDetailLink, 0) as CompanyDetailModel;

                                        key = KapTable.Company.ToString() + "/" + m.Code;
                                        m.Timestamp = long.Parse(DateTime.Now.ToString("yyyyMMddHHmmssfff"));
                                        firebase.Execute<IModel>(key, m);
                                        Thread.Sleep(5000);
                                    }

                                    CompanyList = firebase.Get<CompanyModel>(KapTable.Company.ToString());
                                    firebaseValues = CompanyList.Values.ToList();
                                    newList = models.Except(firebaseValues, new IdComparer()).ToList();

                                    if (newList.Count == 0)
                                        KapProcess.GetInstance().Ready = true;

                                    foreach (var model in models)
                                    {
                                        var m = model as CompanyModel;
                                        if (m.CompanyType == 1)
                                            m.DetailModel = companyDetailParser.Execute(m.CompanyDetailLink, 0) as CompanyDetailModel;
                                        else
                                            m.DetailModel = otherCompanyDetailParser.Execute(m.CompanyDetailLink, 0) as CompanyDetailModel;

                                        key = KapTable.Company.ToString() + "/" + m.Code;
                                        m.Timestamp = long.Parse(DateTime.Now.ToString("yyyyMMddHHmmssfff"));
                                        firebase.Execute<IModel>(key, m);
                                        Thread.Sleep(5000);
                                    }

                                   // if (codes.Count == firebaseCodes.Count && !hasEmptyDetail)
                                    KapProcess.GetInstance().Ready = true;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            ScreenLog(ex.ToString(), ELogType.Information);
                            log.Error(ex);
                        }
                        ScreenLog(String.Format("Company Process Ended. Found: {0} Companies", models.Count), ELogType.Information);
                    }));
                }
                else
                {
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                ScreenLog(typeof(CompanyProcess).ToString() + " " + ex.ToString(), ELogType.Stop);
                log.Error(ex);
                processTask = null;
                return false;
            }
        }

        public bool Stop()
        {
            try
            {
                Pause = true;
                this.timer.Change(Timeout.Infinite, Timeout.Infinite);
                this.timer = null;
                return Pause;
            }
            catch
            {
                return false;
            }
        }

        private void ScreenLog(string message, ELogType process)
        {
            if (OnLogEvent != null)
                OnLogEvent(message, process);
        }
    }

    public class IdComparer : IEqualityComparer<CompanyModel>
    {
        public int GetHashCode(CompanyModel co)
        {
            if (co == null)
            {
                return 0;
            }
            return co.Code.GetHashCode();
        }

        public bool Equals(CompanyModel x1, CompanyModel x2)
        {
            if (object.ReferenceEquals(x1, x2))
            {
                return true;
            }
            if (object.ReferenceEquals(x1, null) ||
                object.ReferenceEquals(x2, null))
            {
                return false;
            }

            if (x1.DetailModel == null || x2.DetailModel == null)
            {
                return true;
            }

            return (x1.Code == x2.Code &&
                    x1.IsDelete == x2.IsDelete &&
                    x1.CompanyDetailLink == x2.CompanyDetailLink &&
                    x1.CompanyName == x2.CompanyName &&
                    x1.CompanyType == x2.CompanyType &&
                    x1.GroupCode == x2.GroupCode &&
                    x1.IndependentAuditCompany == x2.IndependentAuditCompany &&
                    x1.IndependentAuditCompanyLink == x2.IndependentAuditCompanyLink &&
                    x1.Province == x2.Province);
        }
    }
}
