﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParserManager.Model
{
    public enum NotificationType
    {
        DKB = 1,
        FR = 2,
        ODA = 3,
        DG = 4

    }

    public class DKBCompany
    {
        public const string MKK = "MERKEZİ KAYIT KURULUŞU A.Ş.";
        public const string BORSA = "BORSA İSTANBUL A.Ş.";
        public const string TAKAS = "İSTANBUL TAKAS VE SAKLAMA BANKASI A.Ş.";
        public const string KAP = "KAMUYU AYDINLATMA PLATFORMU";
        public const string BORSACIRCUITBREKER = "BORSA İSTANBUL BISTECH DEVRE KESİCİ UYGULAMASI";
    }
}