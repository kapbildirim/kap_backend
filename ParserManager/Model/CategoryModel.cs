﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParserManager.Model
{
    public class CategoryModel
    {
        public int Id { get; set; }
        public bool IsVisible { get; set; }
        public bool IsDelete { get; set; }
        public long Timestamp { get; set; }
        public string Name { get; set; }
        public CategoryModel()
        {
        }
        public CategoryModel(int id, bool isVisible)
        {
            Id = id;
            IsVisible = IsVisible;
        }
    }
}
