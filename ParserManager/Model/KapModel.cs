﻿using System;
using System.Collections.Generic;

namespace ParserManager
{
    public class KapModel : IModel
    {
        public List<int> Indices { get; set; }
        public string Date { get; set; }
        public string Type { get; set; }
        public string ShortName { get; set; }
        public string LongName { get; set; }
        public string Link { get; set; }
        public List<string> AttachmentLink { get; set; }
        public string LogoLink { get; set; }
        public string Description { get; set; }
        public string DetailDescription{get;set;}
        public string ModalHeader { get; set; }
        public bool IsDelete { get; set; }
        public long Timestamp { get; set; }
        public int Id { get; set; }

        public KapModel()
        {
            AttachmentLink = new List<string>();
        }
    }
}
