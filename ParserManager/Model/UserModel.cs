﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParserManager.Model
{
    public class UserModel
    {
        public string Device { get; set; }
        public string Token { get; set; }
        //public bool IsIOS { get; set; }
        public Dictionary<string,string> Company { get; set; }
    }
}
