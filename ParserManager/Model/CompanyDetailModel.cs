﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParserManager
{
    public class CompanyDetailModel : IModel
    {
        public string HeadOfAddress { get; set; }
        public string Email { get; set; }
        public string Website{ get; set; }
        public string CompanyDuration{ get; set; }
        public string IndependentAuditCompany { get; set; }
        public string BISTIndices { get; set; }
        public string CompanySector { get; set; }
        public string BISTMarket { get; set; }
        public string GroupCode { get; set; }
        public string CompanyLogoLink { get; set; }

        public List<string> ContactPeople { get; set; }

        public CompanyDetailModel()
        {
            ContactPeople= new List<string>();
        }
     }
}
