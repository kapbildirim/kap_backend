﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParserManager
{
    class BaseModel : IModel
    {
        public string Date { get; set; }
        public string Type { get; set; }
        public string ShortName { get; set; }
        public string LongName { get; set; }
        public string Link { get; set; }
        public string AttachmentLink { get; set; }
        public string LogoLink { get; set; }
        public string Description { get; set; }
        public int Id { get; set; }
    }
}
