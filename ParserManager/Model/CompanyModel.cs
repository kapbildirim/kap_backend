﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParserManager
{
    public class CompanyModel : IModel
    {
        public string Code { get; set; }
        public string CompanyName{ get; set; }
        public string GroupCode { get; set; }
        public string Province { get; set; }
        public string IndependentAuditCompany{ get; set; }
        public string CompanyDetailLink { get; set; }
        public string IndependentAuditCompanyLink { get; set; }
        public CompanyDetailModel DetailModel { get; set; }
        public int CompanyType { get; set; }
        public bool IsDelete { get; set; }
        public long Timestamp { get; set; }
    }
}
