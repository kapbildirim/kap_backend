﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace ParserManager
{
    public class KapDetailParser : IParser<List<IModel>>
    {
        private WebClient webClient;
        private HtmlDocument document;
        private readonly string companyTableMainNode = "column-type7 wmargin";
        private readonly string companyDetailCodeNodeKey = ".//div[@class='comp-cell _04 vtable']";
        private readonly string companyDetailNameNodeKey = ".//div[@class='comp-cell _14 vtable']";
        private readonly string companyDetailGroupCodeNodeKey = ".//div[@class='comp-cell _13 vtable']";
        private readonly string companyDetailProvinceNodeKey = ".//div[@class='comp-cell _12 vtable']";
        private readonly string companyDetailIndependentAuditCompanyNodeKey = ".//div[@class='comp-cell _11 vtable']";

        private readonly string baseUrl = "https://www.kap.org.tr";

        public string BaseURL { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public KapDetailParser()
        {
            webClient = new WebClient();
            webClient.Encoding = Encoding.UTF8;
            document = new HtmlDocument();
        }
        public HtmlNode findTable()
        {
            HtmlNode node = null;
            foreach (var item in document.DocumentNode.Descendants())
            {
                if (item.Name == "div" && item.OuterHtml.Contains(companyTableMainNode))
                {
                    node = item;
                    break;
                }
            }
            return node;
        }

        public List<IModel> Execute(string url, int Id)
        {
            var modelList = new List<IModel>();
            var detailModel = new CompanyDetailModel();
            Uri url2 = new Uri(url);
            string html = webClient.DownloadString(url2);
            document.LoadHtml(html);
            HtmlNode node = findTable();
            var tableNode = node.SelectNodes("//div[contains(@class,'w-clearfix w-inline-block comp-row')]");

            foreach (HtmlNode div in tableNode)
            {
                string[] codes = getCode(div).Split(',');
                string[] groupCode = getGroupCode(div).Split(',');
                for (int i=0;i< codes.Count(); ++i)
                {
                    CompanyModel model = new CompanyModel();
                    model.Code = codes[i].Trim();
                    model.CompanyDetailLink = getCodeDetailLink(div);
                    model.CompanyName = getCompanyName(div);
                    model.GroupCode = groupCode.Count() == codes.Count() ? groupCode[i].Trim() : groupCode[0].Trim();
                    model.Province = getProvince(div);
                    model.IndependentAuditCompany = getIndependentAuditCompany(div);
                    model.IndependentAuditCompanyLink = getIndependentAuditCompanyLink(div);
                    model.CompanyType = Id;
                    //model.DetailModel = getCompanyDetailModel(model.CompanyDetailLink);
                    modelList.Add(model);
                }
            }

            return modelList;
        }

        private string getCode(HtmlNode item)
        {
            try
            {
                var code = string.Empty;
                var codeNode = item.SelectNodes(companyDetailCodeNodeKey);
                if (codeNode != null)
                {
                    code = codeNode.Descendants("a").FirstOrDefault().InnerText;
                }
                return string.Join("", Regex.Split(code, @"(?:\r\n|\n|\r)")).Replace("&nbsp;", "").Trim();
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        private string getCodeDetailLink(HtmlNode item)
        {
            try
            {
                var codeDetailLink = string.Empty;
                var codeDetailLinkNode = item.SelectNodes(companyDetailCodeNodeKey);
                if (codeDetailLinkNode != null)
                {
                    codeDetailLink = codeDetailLinkNode.Descendants("a").FirstOrDefault().Attributes["href"].Value;
                }
                return baseUrl + codeDetailLink;
            }
            catch (Exception ex)
            {
                return "";
            }
        }
        private string getCompanyName(HtmlNode item)
        {
            try
            {
                var companyName = string.Empty;
                var companyNameNode = item.SelectNodes(companyDetailNameNodeKey);
                if (companyNameNode != null)
                {
                    companyName = companyNameNode.Descendants("a").FirstOrDefault().InnerText;
                }
                return string.Join("", Regex.Split(companyName, @"(?:\r\n|\n|\r)")).Replace("&nbsp;", "").Trim();
            }
            catch (Exception ex)
            {
                return "";
            }
        }
        private string getGroupCode(HtmlNode item)
        {
            try
            {
                var groupCode = string.Empty;
                var groupCodeNode = item.SelectSingleNode(companyDetailGroupCodeNodeKey);
                if (groupCodeNode != null)
                {
                    groupCode = groupCodeNode.ChildNodes[1].InnerText;
                }
                return string.Join("", Regex.Split(groupCode, @"(?:\r\n|\n|\r)")).Replace("&nbsp;", "").Trim();
            }
            catch (Exception ex)
            {
                return "";
            }
        }
        private string getProvince(HtmlNode item)
        {
            try
            {
                var province = string.Empty;
                var provinceCode = item.SelectSingleNode(companyDetailProvinceNodeKey);
                if (provinceCode != null)
                {
                    province = provinceCode.ChildNodes[1].InnerText;
                }
                return string.Join("", Regex.Split(province, @"(?:\r\n|\n|\r)")).Replace("&nbsp;", "").Trim();
            }
            catch (Exception ex)
            {
                return "";
            }
        }
        private string getIndependentAuditCompany(HtmlNode item)
        {
            try
            {
                var independentAuditCompany = string.Empty;
                var independentAuditCompanyNode = item.SelectNodes(companyDetailIndependentAuditCompanyNodeKey);
                if (independentAuditCompanyNode != null)
                {
                    independentAuditCompany = independentAuditCompanyNode.Descendants("a").FirstOrDefault().InnerText;
                }
                return string.Join("", Regex.Split(independentAuditCompany, @"(?:\r\n|\n|\r)")).Replace("&nbsp;", "").Trim();
            }
            catch (Exception ex)
            {
                return "";
            }
        }
        private string getIndependentAuditCompanyLink(HtmlNode item)
        {
            try
            {
                var independentAuditCompanyLink = string.Empty;
                var independentAuditCompanyLinkNode = item.SelectNodes(companyDetailIndependentAuditCompanyNodeKey);
                if (independentAuditCompanyLinkNode != null)
                {
                    independentAuditCompanyLink = independentAuditCompanyLinkNode.Descendants("a").FirstOrDefault().Attributes["href"].Value;
                }
                return baseUrl + independentAuditCompanyLink;
            }
            catch (Exception ex)
            {
                return "";
            }
        }
    }
}

