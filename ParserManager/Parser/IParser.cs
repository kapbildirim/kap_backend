﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParserManager
{
    public interface IParser<T>
    {
        T Execute(string url,int Id);
        string BaseURL { get; set; }
    }
}
