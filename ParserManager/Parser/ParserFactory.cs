﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParserManager
{
    public class ParserFactory<T>
    {
        static Dictionary<string, T> cachedValues = new Dictionary<string, T>();
        static T api = default(T);

        public static T GetParser()
        {
            Type p = typeof(T);
            string key = p.FullName;
            if (cachedValues.TryGetValue(key, out api))
                return api;
            else
            {
                Type type = Type.GetType(p.FullName);
                try
                {
                    api = (T)Activator.CreateInstance(type);
                    cachedValues[key] = api;
                    return api;
                }
                catch (Exception ex)
                {
                    throw new ArgumentException(type + " Nesnesi Oluşturulamadı." + ">" + ex.ToString());
                }
            }
        }
    }
}
