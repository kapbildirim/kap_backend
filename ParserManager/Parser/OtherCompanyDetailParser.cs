﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ParserManager.Parser
{
   public class OtherCompanyDetailParser : IParser<IModel>
    {
        public string BaseURL { get; set; }
        private WebClient webClient;
        private HtmlDocument document;
        private readonly string companyDetailNodeKey = ".//div[@class='w-col w-col-8 w-clearfix sub-col']";
        private readonly string companyDetailSubItemNodeKey = ".//div[@class='comp-cell-row-div vtable infoColumn backgroundThemeForValue']";
        private readonly string companyDetailRowNodeKey = "//div[@class='w-clearfix w-inline-block a-table-row infoRow']";
        private readonly string companyLogoContainer = "w-section middle-section";
        private readonly string companyDetailNode = "w-row sub-row";
        private readonly string baseUrl = "https://www.kap.org.tr";

        public OtherCompanyDetailParser()
        {
            webClient = new WebClient();
            webClient.Encoding = Encoding.UTF8;
            document = new HtmlDocument();
        }

        public IModel Execute(string url, int Id)
        {
            Uri url2 = new Uri(url);
            string html = webClient.DownloadString(url);
            document.LoadHtml(html);
            HtmlNode node = findDetail();
            var companyDetailModel = new CompanyDetailModel();

            var detailNode = node.SelectNodes(companyDetailNodeKey).FirstOrDefault().SelectNodes(companyDetailRowNodeKey);

            companyDetailModel.HeadOfAddress = getCompanyDetailSubItem(detailNode[0]);
            companyDetailModel.Email = getCompanyDetailSubItem(detailNode[1]);
            companyDetailModel.Website = getCompanyDetailSubItem(detailNode[2]);
            companyDetailModel.CompanyDuration = getCompanyDetailSubItem(detailNode[3]);
            companyDetailModel.IndependentAuditCompany = getCompanyDetailSubItem(detailNode[4]);
            companyDetailModel.CompanyLogoLink = getCompanyLogoLink(document);
          
            return companyDetailModel;
        }

        private string getCompanyDetailSubItem(HtmlNode item)
        {
            try
            {
                var subItem = string.Empty;
                var subItemNode = item.SelectNodes(companyDetailSubItemNodeKey);
                if (subItemNode != null)
                {
                    subItem = subItemNode.FirstOrDefault().InnerText;
                }
                return string.Join("", Regex.Split(subItem, @"(?:\r\n|\n|\r)")).Replace("&nbsp;", "").Trim();
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        private string getCompanyLogoLink(HtmlDocument document)
        {
            HtmlNode node = null;
            foreach (var item in document.DocumentNode.Descendants())
            {
                if (item.Name == "div" && item.OuterHtml.Contains(companyLogoContainer))
                {
                    node = item;
                    break;
                }
            }

            string logoLink = baseUrl + node.SelectSingleNode(".//img[@src]").Attributes["src"].Value;
            return logoLink;
        }

        public HtmlNode findDetail()
        {
            HtmlNode node = null;
            foreach (var item in document.DocumentNode.Descendants())
            {
                if (item.Name == "div" && item.OuterHtml.Contains(companyDetailNode))
                {
                    node = item;
                    break;
                }
            }
            return node;
        }
    }
}