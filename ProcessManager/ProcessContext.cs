﻿using log4net;
using LogProvider;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ProcessManager
{
    public class ProcessContext
    {
        object objectLock = new Object();
        Task task = null;
        event ProcessHandler OnProcessStatusChangedEvent;
        event ProcessHandler OnExceptionEvent;
        private static KAPProcess bistProcess;
        private static object islocked = new object();
        private bool isStop = false;
        private int exceptionCount = 0;
        public int? ExceptionTolerance { get; set; }
        public string ProcessName { get; set; }
        public int SleepMillisecond { get; set; }
        public int? Criteria { get; set; }
        public string ProcedureName { get; set; }
        private string statement;
        private ILog log = LogManager.GetLogger(typeof(ProcessContext));
        public volatile bool isRunning = false;
        public event Action<string, ELogType> OnLogEvent;

        public ProcessContext(string processName, string procedureName, int? criteria, int sleepMillisecond, int? exceptionTolerance)
        {
            ProcessName = processName;
            ProcedureName = procedureName;
            SleepMillisecond = sleepMillisecond;
            Criteria = criteria;
            ExceptionTolerance = exceptionTolerance;
            StringBuilder sb = new StringBuilder();
            sb.Append(procedureName);

            if (criteria != null)
            {
                sb.Append(" ");
                sb.Append(criteria);
            }

            statement = sb.ToString();
        }

        public void SetProcessStatus(bool isWorking)
        {
            isStop = isWorking ? false : true;
        }

        public void StartAsyncTask()
        {
            if (isRunning == true) return;

            if (task == null || task.Status == TaskStatus.RanToCompletion || task.Status == TaskStatus.Faulted)
            {
                task = Task.Factory.StartNew(() =>
                {
                    try
                    {
                        //DataConnection SqlQuery = new DataConnection();
                        //OnProcessStatus(ProcessName, ProcessWorking.Always);
                        //while (!isStop)
                        //{
                        //    isRunning = true;
                        //    DataSet ds = SqlQuery.Execute(statement);

                        //    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                        //    {
                        //        ScreenLog(ds.Tables[0].Rows[0][0].ToString(), ELogType.Error);
                        //    }

                        //    exceptionCount = 0;
                        //    Thread.Sleep(SleepMillisecond);
                        //}
                        isRunning = false;
                        OnProcessStatus(ProcessName, ProcessWorking.Never);
                    }
                    catch (Exception ex)
                    {
                        task = null;
                        isRunning = false;
                        ++exceptionCount;
                        Thread.Sleep(1000);
                        if (ExceptionTolerance != null && exceptionCount >= ExceptionTolerance)
                        {
                            OnException(ProcessName, ProcessWorking.Never, ex.ToString());
                            OnProcessStatus(ProcessName, ProcessWorking.Never);
                        }
                        else
                        {
                            OnException(ProcessName, ProcessWorking.Never, ex.ToString());
                            OnProcessStatus(ProcessName, ProcessWorking.Never);
                            StartAsyncTask();
                        }
                    }
                });
            }
        }

        public void OnProcessStatus(string processType, ProcessWorking processStatus)
        {
            try
            {
                ProcessHandler handler = OnProcessStatusChangedEvent;
                if (handler != null)
                {
                    handler(this, new ProcessEventArgs(processType, processStatus));
                }
            }
            catch (Exception ex)
            {
                ScreenLog(ex.ToString(), ELogType.Error);
            }
        }

        public void OnException(string processType, ProcessWorking processStatus, string exception)
        {
            try
            {
                ProcessHandler handler = OnExceptionEvent;
                if (handler != null)
                {
                    handler(this, new ProcessEventArgs(processType, processStatus, exception));
                }
            }
            catch (Exception ex)
            {
                ScreenLog(ex.ToString(), ELogType.Error);
            }
        }

        public event ProcessHandler OnProcessStatusChanged
        {
            add
            {
                lock (objectLock)
                {
                    OnProcessStatusChangedEvent += value;
                }
            }
            remove
            {
                lock (objectLock)
                {
                    OnProcessStatusChangedEvent -= value;
                }
            }
        }

        public event ProcessHandler OnExceptionOccured
        {
            add
            {
                lock (objectLock)
                {
                    OnExceptionEvent += value;
                }
            }
            remove
            {
                lock (objectLock)
                {
                    OnExceptionEvent -= value;
                }
            }
        }

        private void ScreenLog(string message, ELogType process)
        {
            if (OnLogEvent != null)
                OnLogEvent(message, process);
        }

    }
}
