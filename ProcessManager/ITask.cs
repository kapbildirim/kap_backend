﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using log4net;
using Cache;

namespace ProcessManager
{
    public delegate void ProcessHandler(object sender, ProcessEventArgs e);

    public class ProcessEventArgs : EventArgs
    {
        public string Type { get; set; }
        public ProcessWorking Status { get; set; }
        public string Exception { get; set; }

        public ProcessEventArgs(string type, ProcessWorking status)
        {
            Type = type;
            Status = status;
        }

        public ProcessEventArgs(string type, ProcessWorking status, string exception)
        {
            Type = type;
            Status = status;
            Exception = exception;
        }

        public ProcessEventArgs(AppMessageType type, ProcessWorking status)
        {
            Type = type.ToString();
            Status = status;
        }

        public ProcessEventArgs(AppMessageType type, ProcessWorking status, string exception)
        {
            Type = type.ToString();
            Status = status;
            Exception = exception;
        }
    }

    public interface ITask
    {
        event ProcessHandler OnProcessStatusChanged;
        event ProcessHandler OnExceptionOccured;

        void StopGetClOrdIDs();
        void StopProcesses();
        void StartProcesses();
        void SetProcessStatus(AppMessageType process, bool status);
    }

    public class KAPProcess : ITask
    {
        object objectLock = new Object();
        Task VeriDuzenleTask = null;

        event ProcessHandler OnProcessStatusChangedEvent;
        event ProcessHandler OnExceptionEvent;

        private static KAPProcess bistProcess;
        private static object islocked = new object();
        ILog log;

        public static KAPProcess GetInstance()
        {
            if (bistProcess == null)
            {
                lock (islocked)
                {
                    if (bistProcess == null)
                    {
                        bistProcess = new KAPProcess();
                    }
                }
            }
            return bistProcess;
        }

        public void SetProcessStatus(AppMessageType process, bool status)
        {
            ProcessWorking s = status ? ProcessWorking.Always : ProcessWorking.Never;
            if (s == ProcessWorking.Never)
            {
                switch (process)
                {
                    case AppMessageType.TodayToHistory:
                        stopVeriDuzenle = true;
                        break;
                }
            }
            else
            {
                switch (process)
                {
                    case AppMessageType.TodayToHistory:
                        stopVeriDuzenle = false;
                        StartAsyncTodayToHistory();
                        break;
                }
            }
        }

        public void StopProcesses()
        {
            stopVeriDuzenle = true;
        }

        public void StartProcesses()
        {
            bool isProcessActive = false;
            object basePath = FileCache.Get<object>(AppMessageType.TodayToHistory.ToString());
            isProcessActive = (bool)basePath;
            SetProcessStatus(AppMessageType.TodayToHistory, isProcessActive);
        }

        private KAPProcess()
        {
            log = LogManager.GetLogger(typeof(KAPProcess));
        }

        private bool stopVeriDuzenle = false;
        private int VDExceptionCount = 0;
        public void StartAsyncTodayToHistory()
        {
            if (VeriDuzenleTask == null || VeriDuzenleTask.Status == TaskStatus.RanToCompletion || VeriDuzenleTask.Status == TaskStatus.Faulted)
            {
                VeriDuzenleTask = Task.Factory.StartNew(() =>
                {
                    try
                    {
                        OnProcessStatus(AppMessageType.TodayToHistory, ProcessWorking.Always);
                        //while (!stopVeriDuzenle)
                        //{
                        //    DataSet ds = SqlQuery.Execute("up_FIXProcesses 2");

                        //    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                        //    {
                        //        logActivity.Write(new ScreenLog(MethodBase.GetCurrentMethod().GetFullNameWithException(new Exception(ds.Tables[0].Rows[0][0].ToString())), ELogMessageType.Error));
                        //    }

                        //    VDExceptionCount = 0;
                        //    Thread.Sleep(30000);
                        //}
                        OnProcessStatus(AppMessageType.TodayToHistory, ProcessWorking.Never);
                    }
                    catch (Exception ex)
                    {
                        VeriDuzenleTask = null;
                        ++VDExceptionCount;
                        if (VDExceptionCount >= 2)
                        {
                            OnException(AppMessageType.TodayToHistory, ProcessWorking.Never, ex.ToString());
                            OnProcessStatus(AppMessageType.TodayToHistory, ProcessWorking.Never);
                        }
                        else
                        {
                            OnException(AppMessageType.TodayToHistory, ProcessWorking.Never, ex.ToString());
                            OnProcessStatus(AppMessageType.TodayToHistory, ProcessWorking.Never);
                            StartAsyncTodayToHistory();
                        }
                    }
                });
            }
        }

        public void StopGetClOrdIDs()
        {
            //stopClOrdID = true;
        }

        public void OnProcessStatus(AppMessageType processType, ProcessWorking processStatus)
        {
            try
            {
                ProcessHandler handler = OnProcessStatusChangedEvent;
                if (handler != null)
                {
                    handler(this, new ProcessEventArgs(processType, processStatus));
                }
            }
            catch (Exception ex)
            {
                //logActivity.Write(new ScreenLog(MethodBase.GetCurrentMethod().GetFullNameWithException(ex), ELogMessageType.Error));
            }
        }

        public void OnException(AppMessageType processType, ProcessWorking processStatus, string exception)
        {
            try
            {
                ProcessHandler handler = OnExceptionEvent;
                if (handler != null)
                {
                    handler(this, new ProcessEventArgs(processType, processStatus, exception));
                }
            }
            catch (Exception ex)
            {
                //logActivity.Write(new ScreenLog(MethodBase.GetCurrentMethod().GetFullNameWithException(ex), ELogMessageType.Error));
            }
        }

        public event ProcessHandler OnProcessStatusChanged
        {
            add
            {
                lock (objectLock)
                {
                    OnProcessStatusChangedEvent += value;
                }
            }
            remove
            {
                lock (objectLock)
                {
                    OnProcessStatusChangedEvent -= value;
                }
            }
        }

        public event ProcessHandler OnExceptionOccured
        {
            add
            {
                lock (objectLock)
                {
                    OnExceptionEvent += value;
                }
            }
            remove
            {
                lock (objectLock)
                {
                    OnExceptionEvent -= value;
                }
            }
        }


    }
}
