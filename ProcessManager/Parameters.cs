﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessManager
{
    public class Parameters
    {
        public const string KapBaseLinkKey = "KapBaseLink";
        public const string KapLinkKey = "KapLink";
        public const string KapLastIdKey = "KapLastId";
        public const string FireBasePathKey = "FireBasePath";
        public const string FireBaseAuthKey = "FireBaseAuth";
        public const string CompanyLinkKey = "CompanyLink";
        public const string CompaniesPeriod = "CompaniesPeriod";
        public const string TodayToHistoryPeriod = "TodayToHistoryPeriod";
        public const string BISTSirketleriYK = "BISTSirketleriYK";
        public const string BISTSirketleriPYS = "BISTSirketleriPYS";
        public const string NotificationKey = "NotificationKey";
        public const string UserProcessPeriod = "UserProcessPeriod";
        public const string OSAppID = "OSAppID";
        public const string OSRestApiKey = "OSRestApiKey";
    }
}
