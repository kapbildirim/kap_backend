﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessManager
{
    public interface IProcess
    {
        bool Start();
        bool Stop();
        bool Pause { get; set; }
    }

    public enum ProcessWorking
    {
        Always = 0,
        Rare = 1,
        Never = 2,
    }

    public enum AppMessageType
    {
        TodayToHistory=1
    }
}
