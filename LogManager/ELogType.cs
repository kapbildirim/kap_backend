﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogProvider
{
    public enum ELogType
    {
        Start = 1,
        Stop = 2,
        Information = 3,
        Error = 4,
        KapLastIdKey = 5,
        UserCount = 6,
    }
}
