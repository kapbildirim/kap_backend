﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;
using System.Text.RegularExpressions;
using log4net;
using ParserManager.Model;

namespace ParserManager
{
    public class KapParser : IParser<IModel>
    {
        private readonly string mainNode = "w-clearfix modal-single disc-type";
        private readonly string imageNodeKey = "//div[@class='modal-headerleft']";
        private readonly string descriptionNodeKey = "//div[@class='disclosureSummary']";
        private readonly string headerNodeKey = "//div[@class='modal-info']/h1/text()";
        private readonly string notificationKey = "//div[@class='w-col w-col-3 modal-briefsumcol']";
        private readonly string attachmentNodeKey = "//div[@class='w-clearfix modal-attachments']";
        private readonly string memberShortNodeKey = "//div[@class='type-medium bi-dim-gray']";
        private readonly string memberLongNameNodeKey = "//div[@class='type-medium type-bold bi-sky-black']";
        private readonly string detailDescriptionNodeKey = "//td[@class='taxonomy-context-value-summernote multi-language-content content-tr']";
        private readonly string descriptionDkbNodeKey = "//td[@class='info-table-title-cell']";
        private readonly string dkbEmptyInfoNodeKey = "//table[@class='tbl_GENEL_DUYURU_GONDERIMI']";

        private string baseURL = "https://www.kap.org.tr/tr";
        private WebClient webClient;
        private HtmlDocument document;
        private ILog log = LogManager.GetLogger(typeof(KapParser));

        public string BaseURL
        {
            get => baseURL;
            set => baseURL = value;
        }

        public KapParser()
        {
            webClient = new WebClient();
            webClient.Encoding = Encoding.UTF8;
            document = new HtmlDocument();
        }

        private string getImage(HtmlNode item)
        {
            try
            {
                string imgUrl = "";
                var imageNode = item.SelectNodes(imageNodeKey);
                if (imageNode != null)
                {
                    var img = imageNode.Descendants("img").First();
                    imgUrl = img.Attributes["src"].Value;
                }
                return imgUrl;
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return "";
            }
        }



        private string getLongName(HtmlNode item, string type)
        {
            try
            {
                string memberLongName = "";
                HtmlNodeCollection memberLongNameNode;
                memberLongNameNode = item.SelectNodes(memberLongNameNodeKey);

                if (type == NotificationType.DKB.ToString())
                {
                    if (memberLongNameNode != null)
                    {
                        memberLongName = memberLongNameNode.FirstOrDefault().SelectSingleNode(".//span[@class='vcell']").InnerText;
                    }
                }
                else
                {
                    if (memberLongNameNode != null)
                    {
                        var aLink = memberLongNameNode.Descendants("a").First();
                        memberLongName = aLink.InnerText;
                    }
                }
                return memberLongName;
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return "";
            }
        }

        private string getShortName(HtmlNode item, string longName, string type)
        {
            try
            {
                string memberShortName = "";
                var memberShortNode = item.SelectNodes(memberShortNodeKey);
                if (memberShortNode != null)
                {
                    memberShortName = memberShortNode.First().InnerText;
                    if (string.IsNullOrWhiteSpace(memberShortName))
                    {
                        if (type == NotificationType.DKB.ToString())
                        {
                            if (longName == DKBCompany.KAP || longName == DKBCompany.MKK)
                            {
                                memberShortNode = item.SelectNodes(dkbEmptyInfoNodeKey);
                                if (memberShortNode != null)
                                {
                                    var shortNameNode = memberShortNode.FirstOrDefault().SelectNodes("//div[@class='gwt-HTML control-label lineheight-32px']");
                                    if (shortNameNode != null)
                                    {
                                        var newString = string.Join("", Regex.Split(shortNameNode.ElementAt(1).InnerText, @"(?:\r\n|\n|\r)"));
                                        memberShortName = newString.Trim();

                                    }
                                }
                            }
                            else
                            {
                                memberShortNode = item.SelectNodes(descriptionDkbNodeKey);
                                var subNode = memberShortNode.FirstOrDefault().SelectSingleNode("//div[@class='gwt-Label']").InnerText;
                                if (!string.IsNullOrWhiteSpace(subNode))
                                {
                                    var newString = string.Join("", Regex.Split(subNode, @"(?:\r\n|\n|\r)"));

                                    newString = newString.Replace("[", "");
                                    newString = newString.Replace("]", "");
                                    memberShortName = newString.Trim();
                                }
                            }

                        }

                    }
                }

                return memberShortName;
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return "";
            }
        }

        private List<string> getAttachment(HtmlNode item)
        {
            try
            {
                var attachmentNode = item.SelectNodes(attachmentNodeKey);
                var attachmentLink = new List<string>();
                if (attachmentNode != null)
                {
                    foreach (var alink in attachmentNode.Descendants("a"))
                    {
                        var link = alink.Attributes["href"].Value.Replace("#", "");
                        if (!string.IsNullOrWhiteSpace(link))
                        {
                            attachmentLink.Add(baseURL + link);
                        }
                    }
                }
                return attachmentLink;
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return new List<string>();
            }
        }

        private string getDate(HtmlNode item)
        {
            try
            {
                string date = "";
                var notification = item.SelectNodes(notificationKey);

                if (notification != null)
                {
                    date = notification[0].ChildNodes[3].InnerText;
                }
                return date;
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return "";
            }
        }

        private string getType(HtmlNode item)
        {
            try
            {
                string type = "";
                var notification = item.SelectNodes(notificationKey);

                if (notification != null)
                {
                    var notificationType = notification[1].ChildNodes[3].InnerText;
                    var newString = string.Join("", Regex.Split(notificationType, @"(?:\r\n|\n|\r)"));
                    type = newString.Trim();
                }

                return type;
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return "";
            }
        }

        private string getDescription(HtmlNode item, string type)
        {
            try
            {
                string description = "";
                HtmlNodeCollection descriptionNode = null;
                descriptionNode = item.SelectNodes(descriptionNodeKey);
                if (descriptionNode != null)
                {
                    description = descriptionNode.FirstOrDefault().InnerText.Replace("&nbsp;", "").Trim();
                    if (description == "-")
                    {
                        description = "";
                    }
                }

                return description;
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return "";
            }
        }

        private string getDetailDescription(HtmlNode item, string longName, string type)
        {
            try
            {
                string description = "";
                var descriptionNode = item.SelectNodes(detailDescriptionNodeKey);

                if (descriptionNode != null)
                {
                    var desc = descriptionNode.FirstOrDefault().InnerText.Replace("&nbsp;", "").Trim();

                    var newString = string.Join("", Regex.Split(desc, @"(?:\r\n|\n|\r)"));
                    description = newString.Trim();
                    if (description == "-")
                    {
                        description = "";
                    }

                }
                else
                {

                    if (type == NotificationType.DKB.ToString())
                    {
                        if (longName == DKBCompany.KAP || longName == DKBCompany.MKK)
                        {
                            descriptionNode = item.SelectNodes(dkbEmptyInfoNodeKey);
                            if (descriptionNode != null)
                            {
                                var newDescriptionNode = descriptionNode.FirstOrDefault().SelectSingleNode("//p[@class='MsoNormal']");
                                if (newDescriptionNode != null)
                                {
                                    var newString = string.Join("", Regex.Split(newDescriptionNode.InnerText, @"(?:\r\n|\n|\r)"));
                                    description = newString.Trim();

                                }
                            }
                        }

                    }

                }

                return description;
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return "";
            }
        }

        private string getModalHeader(HtmlNode item)
        {
            try
            {
                string headerText = "";
                var descriptionNode = item.SelectNodes(headerNodeKey);

                if (descriptionNode != null)
                {
                    var text = descriptionNode.FirstOrDefault().InnerText.Replace("&nbsp;", "").Trim();
                    var newString = string.Join("", Regex.Split(text, @"(?:\r\n|\n|\r)"));
                    headerText = newString.Trim();

                }

                return headerText;
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
                return "";
            }
        }

        public IModel Execute(string url, int Id)
        {
            url = string.Concat(url, Id);
            string html = webClient.DownloadString(url);
            document.LoadHtml(html);
            HtmlNode node = findModal();

            KapModel model = new KapModel();

            if (node.HasChildNodes)
            {
                model.LogoLink = baseURL + getImage(node);
                model.Type = getType(node);
                model.LongName = getLongName(node, model.Type);
                model.ShortName = getShortName(node, model.LongName, model.Type);
                model.AttachmentLink.AddRange(getAttachment(node));
                model.Date = getDate(node);
                model.Description = getDescription(node, model.Type);
                model.DetailDescription = getDetailDescription(node, model.LongName, model.Type);
                model.ModalHeader = getModalHeader(node);
                model.Link = url;
                model.Id = Id;
            }
            return model;
        }

        public HtmlNode findModal()
        {
            HtmlNode node = null;
            foreach (var n in document.DocumentNode.Descendants())
            {
                if (n.Name == "div" && n.OuterHtml.Contains(mainNode))
                {
                    node = n;
                    break;
                }
            }

            return node;
        }
    }
}