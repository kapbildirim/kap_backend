﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Concurrent;

namespace FireManager
{
    public class MessageBag<T>
    {
        ConcurrentDictionary<T, DateTime> messageBag;
        System.Timers.Timer timer;

        public MessageBag()
        {
            IsClear = false;
            messageBag = new ConcurrentDictionary<T, DateTime>();
        }

        public MessageBag(bool isclear)
        {
            IsClear = isclear;
            messageBag = new ConcurrentDictionary<T, DateTime>();
            if (IsClear == true)
            {
                timer = new System.Timers.Timer(60000);
                TimerControl();
                timer.Start();
            }
        }

        private void TimerControl()
        {
            timer.Elapsed += delegate (object _s, System.Timers.ElapsedEventArgs _e)
            {
                Remove();
            };
        }

        public bool Add(T item)
        {
            return messageBag.TryAdd(item, DateTime.Now);
        }

        public bool CanGet(T item)
        {
            DateTime dt;
            return messageBag.TryGetValue(item, out dt);
        }

        public bool Like(T item)
        {
            bool result = false;
            messageBag.Keys.ToList().ForEach(f => {
                if (f.ToString().Contains(item.ToString()))
                {
                    result = true;
                }
            });
            return result;
        }

        private void Remove()
        {
            var keysToRemove = messageBag.Where(kvp => (DateTime.Now - kvp.Value).TotalSeconds > 30)
                        .Select(kvp => kvp.Key)
                        .ToArray();
            DateTime dt;
            foreach (var key in keysToRemove)
            {
                messageBag.TryRemove(key, out dt);
            }
        }

        public bool Stop()
        {
            try
            {
                timer.Stop();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool Start()
        {
            try
            {
                timer.Start();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool Clear()
        {
            try
            {
                messageBag.Clear();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool IsClear { get; set; }

    }
}
