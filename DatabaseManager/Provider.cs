﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using Cache;
using ProcessManager;

namespace FireManager
{
    public class Provider
    {
        //static Dictionary<string, T> cachedValues = new Dictionary<string, T>();

        public static IDBase Execute()
        {
            object basePath = FileCache.Get<object>(Parameters.FireBasePathKey);
            object authSecret = FileCache.Get<object>(Parameters.FireBaseAuthKey);
            object notificationKey = FileCache.Get<object>(Parameters.NotificationKey);
            FirBase provider = new FirBase((string)basePath, (string)authSecret, (string)notificationKey);
            return provider;
        }
    }
}
