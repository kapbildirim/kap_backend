﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FireManager
{
    public enum KapTable
    {
        History = 1,
        Today = 2,
        Company = 3,
        Category = 4,
        User = 5
    }
}
