﻿using FireSharp.Interfaces;
using FireSharp.Config;
using FireSharp;
using FireSharp.Response;
using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using FirebaseNet;
using FirebaseNet.Messaging;

namespace FireManager
{
    class FirBase : IDBase
    {
        private IFirebaseClient client;
        FCMClient fcmClient;
        public IFirebaseClient Client 
        {
            get
            {
                return client;
            }
        }

        public FirBase(string basePath, string authSecret, string messagingKey)
        {
            IFirebaseConfig config = new FirebaseConfig();
            config.AuthSecret = authSecret;
            config.BasePath = basePath;
            client = new FirebaseClient(config);
            fcmClient = new FCMClient(messagingKey);
        }

        public string Execute<T>(string key, T model)
        {
            SetResponse response = client.Set(key, model);
            return response.Body;
        }

        public string Execute<T>(KapTable tableName, string Id, T model)
        {
            SetResponse response = client.Set(tableName.ToString() + "/" + Id, model);
            return response.Body;
        }

        public Dictionary<string, T> Get<T>(string key)
        {
            FirebaseResponse response = client.Get(key);
            return JsonConvert.DeserializeObject<Dictionary<string, T>>(response.Body);
        }

        public List<T> GetList<T>(string key)
        {
            FirebaseResponse response = client.Get(key);
            return JsonConvert.DeserializeObject<List<T>>(response.Body);
        }

        public string Delete<T>(string key)
        {
            FirebaseResponse response = client.Delete(key);
            return response.Body;
        }

        public void SendNotification(string to,string body, string title, string icon, bool isIOS)
        {
            Message message = null;
            if (!isIOS)
            {
                message = new Message()
                {
                    To = to,
                    Notification = new AndroidNotification()
                    {
                        Body = body,
                        Title = title,
                        Icon = icon
                    }
                };
            }
            else
            {
                message = new Message()
                {
                    To = to,
                    Notification = new IOSNotification()
                    {
                        Body = body,
                        Title = title
                    }
                };
            }

            var result = fcmClient.SendMessageAsync(message);
        }
    }
}
