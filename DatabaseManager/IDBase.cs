﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FireManager
{
    public interface IDBase
    {
        string Execute<T>(string key, T model);
        string Execute<T>(KapTable tableName,string Id, T model);
        Dictionary<string, T> Get<T>(string key);
        List<T> GetList<T>(string key);
        string Delete<T>(string key);
        void SendNotification(string to, string body, string title, string icon, bool isIOS);
    }
}
