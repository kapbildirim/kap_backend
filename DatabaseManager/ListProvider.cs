﻿using System;
using System.Collections.Generic;

namespace FireManager
{
    public enum ListType
    {
        CompanyList = 1,
    }

    public class ListProvider<T>
    {
        static Dictionary<string, T> cachedValues = new Dictionary<string, T>();
        static T api = default(T);

        public static T GetApiProvider(ListType type)
        {
            Type p = typeof(T);
            string key = p.FullName + type.ToString();
            if (cachedValues.TryGetValue(key, out api))
                return api;
            else
            {
                Type readerType = Type.GetType(p.FullName);
                try
                {
                    api = (T)Activator.CreateInstance(readerType);
                    cachedValues[key] = api;
                    return api;
                }
                catch (Exception ex)
                {
                    throw new ArgumentException(readerType + " Nesnesi Oluşturulamadı." + ">" + ex.ToString());
                }
            }
        }

        public static T GetApiProvider()
        {

            Type p = typeof(T);
            string key = p.FullName;
            if (cachedValues.TryGetValue(key, out api))
                return api;
            else
            {
                Type readerType = Type.GetType(p.FullName);
                try
                {
                    api = (T)Activator.CreateInstance(readerType);
                    cachedValues[key] = api;
                    return api;
                }
                catch (Exception ex)
                {
                    throw new ArgumentException(readerType + " Nesnesi Oluşturulamadı." + ">" + ex.ToString());
                }
            }
        }
    }
}
